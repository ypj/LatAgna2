// fhandle.h
#ifndef __FHANDEL_H__
#define __FHANDEL_H__

#include <stdio.h>
//#include <iostream>
#include <cstdlib>
#include <cstdarg>
#include <hdf5.h>

//using namespace std;
  
typedef struct{
  double re;
  double im;
}tslice;


class fhandle{
  public:
    fhandle();
    ~fhandle();
    void open(char*,char*,char* fmt);
    void open(char*,char*);
    void h5dopen(char*);
    void close(void);
    void h5dclose(void);
    char* name(void);
    char* fmt(void);
    void skipLine(int);
    void parseLine(const char*, ...);
    int parseIndexedVarField(double*, int);
    int testLine(const char*, ...);
    void h5dread_alloc_workspace(int);
    void h5dread_free_workspace();
    tslice* h5dread_compound_tslice(void);
    void appendData(int,double*,const int);
    void appendData(int,double);
    void appendData(int,double,double);
    void appendData(int,double,double,double);
    void appendData(int,double,double,double,double);
    void appendData(int,double,double,double,double,double);
    void appendData(int,double,double,double,double,double,double);
    void put(const char*, ...);
  
  private:
    const char *clsname; 
    char *filename;
    char *filemode;
    char *fileformat;
    FILE *fp;
    char *datasetname;
    hid_t h5fid, h5gid, h5did;
    hid_t memtype;
    tslice *h5buf;
};

#endif
