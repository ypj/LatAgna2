// error.h
#ifndef __ERROR_H__
#define __ERROR_H__

#include <cstdio>
#include <cstdlib>
#include <cstdarg>

void putMsg(const char*, const char*, ...);
void dbgMsg(const char*, const char*, ...);
void warnMsg(const char*, const char*, ...);
void errExit(const int, const char*, const char*, ...);

#endif
