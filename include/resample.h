// resample.h
#ifndef __RESAMPLE_H__
#define __RESAMPLE_H__

typedef enum DATATYPE{
  RAW,
  RAWJK
} DATATYPE;

int resampleJKfromRAW(int del, multi1d<double>& data, 
                      multi1d<double>& jkm, multi1d<double>& jkv);

int resampleJKfromFIT(int del, multi1d<double>& data, 
                      multi1d<double>& jkm, multi1d<double>& jkv);

int resampleJKfromRAWJK(int del, multi1d<double>& data, 
                        multi1d<double>& jkm, multi1d<double>& jkv);

void estimateJk(multi1d<double>& x, double& avg, double& err);

void estimateMeanStd(multi1d<double>& x, double& avg, double& err);

void estimateJkVec(multi1d<double>& xv, 
                   multi1d<double>& avgv, multi1d<double>& errv);

void estimateJkCov(multi1d<double>& xv, multi1d<double>& covmat);

#endif
