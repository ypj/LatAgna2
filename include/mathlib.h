// mathlib.h
#ifndef __MATHLIB_H__
#define __MATHLIB_H__

#include <cmath>

double coshinv(double r, double t, int nt2);
double sinhinv(double r, double t, int nt2);
double cothinv(double ratt, double y, double yf);
double tanhinv(double ratt, double y, double yf);

#endif
