//nr.h
#ifndef __NR_H__
#define __NR_H__

#include "multi1d.h"
#include <cmath>
#include <algorithm>
#include <limits>

double gammq(double a, double x);

double gammln(double xx);

const int NMAX = 500; // svdcmp
extern double TOL_SVD_INV;
extern double TOL_SVD_SOL;
//const double TOL_SVD = 1.0e-70;// garfield [test] diagonal matrix
//const double TOL_SVD = 2.0e-1; // garfield [test] sample nt=4
//const double TOL_SVD = 2.0e-2; // garfield [test] sample nt=64

int svdcmp( multi1d<double>& a, multi1d<double>& w, multi1d<double>& v );

double pythag( double a, double b );

double sign( double a, double b );

int svbksb( multi1d<double>& u, multi1d<double>& w, multi1d<double>& v, 
            multi1d<double>& b, multi1d<double>& x );

void linmin(multi1d<double>& p, const int n1, const int nt, const char* fit,
	        multi1d<double>&avgtmp, multi1d<double>&varinv,
			multi1d<double>& xi, double *fret);
#if 0
void mnbrak(double *ax, double *bx, double *cx, 
	        double *fa, double *fb, double *fc,
	        double (*func)(double,
			multi1d<double>&, multi1d<double>&,
			const int, const int, const char*,
	        multi1d<double>&, multi1d<double>&),
			multi1d<double>& p, multi1d<double>& xi,
			const int n1, const int nt, const char* fit,
	        multi1d<double>&avgtmp, multi1d<double>&varinv);

double brent(double ax, double bx, double cx, 
			 double tol, double *xmin,
		   	 double (*f)(double,
			 multi1d<double>&, multi1d<double>&,
			 const int, const int, const char*,
	         multi1d<double>&, multi1d<double>&),
			 multi1d<double>& pcom, multi1d<double>& xi,
			 const int n1, const int nt, const char* fit,
	         multi1d<double>&avgtmp, multi1d<double>&varinv);
#else
void mnbrak(double ax, double bx, double *cx, 
	        double *fa, double *fb, double *fc,
			multi1d<double>& p, multi1d<double>& xi,
			const int n1, const int nt, const char* fit,
	        multi1d<double>&avgtmp, multi1d<double>&varinv);

double brent(double ax, double bx, double cx, 
			 double tol, double *xmin,
			 multi1d<double>& pcom, multi1d<double>& xi,
			 const int n1, const int nt, const char* fit,
	         multi1d<double>&avgtmp, multi1d<double>&varinv);
#endif
double f1dim(double x, multi1d<double>& p, multi1d<double>& xi,
			 const int n1, const int nt, const char* fit,
	         multi1d<double>&avgtmp, multi1d<double>&varinv);

#endif
