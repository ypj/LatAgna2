// fitlib.h
#ifndef __FIT_H__
#define __FIT_H__

#include "multi1d.h"
#include "fitparam.h"

int covfit(int n1, const char* fit, 
           multi1d<double>& avg, multi1d<double>& varinv,
           multi1d<double>& c, 
           int *c_guess, double *inv_width,
           double& cdf, double& q,
           char* MINIMIZER, bool corflag,
           int& nsvd, int& maxmode, int nbayes, int& namoeba);

int covfit_smt(int n_data, int n1_v[], char fit[][FITSTRLEN],  
               multi1d<double>& avg, int nt_v[], 
               multi1d<double>& varinv, int npt_v[],
               multi1d<double>& c, 
               int *c_guess, double *c_prior, double *inv_width,
               double& cdf, int dof, double& q,
               char* MINIMIZER, bool corflag);

void effAmplitude(multi1d<double>& c, char* fit, const int nt,
                  multi1d<double>& corr, multi1d<double>& amp);

void effMass(multi1d<double>& avg, 
             const int nt, const char* fit,
             bool symmflag, multi1d<double>& rat);
//void effMass(multi1d<double>& avg, multi1d<double>& var, 
//             const int n1, const int nt, const char* fit,
//             bool symmflag, multi1d<double>& rat);

void avgEffMass(multi1d<double>& avg, 
             const int nt, const char* fit, double M_fit,
             bool symmflag, multi1d<double>& rat);
#endif
