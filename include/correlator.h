// correlator.h
#ifndef __CORRELATOR_H__
#define __CORRELATOR_H__

#include "fhandle.h"
#include "multi1d.h"

int symmetrize(mdouble& data);
int symmetrize(mdouble& data, int nt, int t_offset, char* symm_type);

#endif
