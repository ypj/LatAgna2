// fitlib.h
#ifndef __FITLIB_H__
#define __FITLIB_H__

#include <cstring>
#include "multi1d.h"
#include "fitparam.h"

int NumParameters(char fit[][FITSTRLEN], int npart);
int NumParameters(const char *fit);

void numStates(const char *fit, int state[]);

int meffIdx(const char *fit);

void f_value(multi1d<double>& c, multi1d<double>& y,
             const int n1, const int nt, const char *fit);

double gfit(const char *fit, int it, const int nt, multi1d<double>& c);

double gdfit(const char *fit, int it, const int nt, multi1d<double>& c_arg, 
	         int a);

double gddfit(const char *fit, int it, const int nt, 
	          multi1d<double>& c_arg, int a, int b);

double ChiSquared(mdouble&c, const int n1, const int nt, const char* fit,
				  mdouble& avgtmp, mdouble& varinv, 
				  bool prior = true);

// garfield [modify] deprecated
//double ChiSquared(multi1d<double>& avgtmp, 
//				  multi1d<double>& avgtrue,
//				  multi1d<double>& varinv);

double dChiSquared(multi1d<double>& c, 
				   const int n1, const int nt, const char *fit,
				   multi1d<double>& avgtmp, multi1d<double>& varinv,
				   int a,
				   bool prior = true);

double ddChiSquared(multi1d<double>& c, 
				   const int n1, const int nt, const char *fit,
				   multi1d<double>& avgtmp, multi1d<double>& varinv, 
				   int a, int b,
				   bool prior = true);

int setBasis(mdouble& u, const int n1, const int nt, const char* fit);

int convertToMass(multi1d<double>& c, const char *fit);

#endif
