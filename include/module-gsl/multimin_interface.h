#ifndef __INTERFACE_GSL_MULTIMIN_H__
#define __INTERFACE_GSL_MULTIMIN_H__

#include "multi1d.h"
#include "gslparam.h"
#include "func_fit.h"

void GSL_multiMinChiSq(mdouble& c_in, double iw_in[], const int n1, const int nt, const char *fitfunc, int& iter, double& chi2, mdouble& avgtmp, mdouble& varinv);

void GSL_multiMinChiSq(mdouble& c_in, double p_in[], double iw_in[], const int n1_v[], const int nt_v[], const int npt_v[], const int npart, char fitfunc[][100], int& iter, double& chi2, mdouble& avgtmp, mdouble& varinv);

#endif
