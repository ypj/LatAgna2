#ifndef __MODULE_GSL_FUNC_CHISQ_H__
#define __MODULE_GSL_FUNC_CHISQ_H__

#include <vector>
#include <functional>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

struct FitData{
  int n1;
  int n2;
  int nt;
  gsl_vector *avg;
  gsl_matrix *invcov;
  int nparam;
  int dim_min_dom; // dimension of minimizer domain
  int *map_dom;     // minimizer domain -> full parameter domain
  int *inv_map_dom; // full parameter domain -> minimizer domain (not 1-1) 
  int *amp;
  char func[100];
  gsl_vector *true_c;
  gsl_vector *delta;
  gsl_matrix *grad;
  gsl_vector *aux_vec;
  gsl_vector *g;
  gsl_vector *p;
  gsl_vector *iw;
  
  int *n1_v;
  int *n2_v;
  int *nt_v;
  int *npt_v;
  int *npar_v;
  int npart;
  char func_v[50][100];
  std::vector<std::function<double(const double*,int)>> fitFunc;
  std::vector<std::function<double(const double*,int,int)>> fitFuncDeriv;
};

double _GSL_chiSq(const gsl_vector *params, void *fit);
void _GSL_dchiSq(const gsl_vector *params, void *fit, gsl_vector *dchisq);
void _GSL_chiSq_dchiSq(const gsl_vector *params, void *fit, double *chisq, gsl_vector *dchisq);

#endif
