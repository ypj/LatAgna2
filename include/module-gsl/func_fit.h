// func_fit.h
#ifndef __MODULE_GSL_FUNC_FIT_H__
#define __MODULE_GSL_FUNC_FIT_H__

#include <functional>

void GSL_numStates(const char *fit, int state[], int* symflag);
int GSL_numParams(const char *fit);
int GSL_haveOppParity(const char *fit);
int GSL_isSymmetric(const char *fit);
int GSL_isNpt(const char *fit);
double GSL_fitFunc(const double *c, int i, const int nt, const char *fit); 
std::function<double(const double*,int)> GSL_bindFitFunc(const int nt, const char *fit); 
double GSL_da_dfitFunc(const double *c, int a, int i, const int nt, 
                       const char *fit);
std::function<double(const double*,int,int)> GSL_bindFitFuncDeriv(const int nt, const char *fit);

#endif
