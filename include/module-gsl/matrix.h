#ifndef __MODULE_GSL_MATRIX_H__
#define __MODULE_GSL_MATRIX_H__

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_matrix.h>


//-------------------------------------------------------------------------
// GSL Matrix operation wrappers
//-------------------------------------------------------------------------
// NOTE: support only a square matrix
//-------------------------------------------------------------------------

// return trace of matrix A
double traceMat(gsl_matrix *matA, int n);

// matrix multiplication A*B updates matrix B; matrix A dose not altered.
int multMat(gsl_matrix *matA, gsl_matrix *matB, int n);

// print matrix A
int printMat(gsl_matrix *matA, int n);

// add a constant multiple of the identity matrix to the matrix A
int addMatConstMultId(gsl_matrix *matA, double c, int n);

#endif
