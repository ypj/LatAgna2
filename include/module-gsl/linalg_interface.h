#ifndef __INTERFACE_GSL_LINALG_H__
#define __INTERFACE_GSL_LINALG_H__

#include "multi1d.h"

int GSL_svdcmp_jacobi(mdouble& u_mat, mdouble& diag, mdouble& v_mat, 
                      double tol, int & nsvd, int& maxmode);

#endif
