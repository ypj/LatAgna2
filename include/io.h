// io.h
#ifndef __IO_H__
#define __IO_H__

#include <cstring>
#include "fhandle.h"
#include "multi1d.h"

int readData(mdouble& data, fhandle& fin, bool swap_re_im=false);
int readData(mdouble& data, fhandle& fin, int nt, int t_offset, 
             bool swap_re_im=false);

int readDataDispersion(mdouble& data, fhandle& fin, const char* ytype);

int readAux(mdouble& data, fhandle& fin, int npa, int offset);

int readFitData(multi1d<double>& c_fit, fhandle& fitfile, 
                int npa, int offset);

int readRawData(mdouble& data, fhandle& fin, mstring& corrkey, bool fresh);

int readFitHeader(int set, fhandle& fitfile, 
                  char *channel, char *fit, 
                  int l[], int u[], int offset);

void writeHeader(const char* corfilename, 
	             const char* fit, int n1, int n2, const char* fullcov,
				 double cdf_fit, double cdf_err, 
				 double q_fit, double q_err,
				 int imass, mdouble& c_fit, mdouble& c_err,
				 fhandle& fout, bool first=true);

void writeData(mdouble& y1, fhandle& fout);

void writeData(mdouble& y1, mdouble& y2, fhandle& fout);

void writeData(mint& x, mdouble& y1, mdouble& y2, fhandle& fout);

void writeData(mdouble& y1, mdouble& y2, 
			   mdouble& y3, mdouble& y4, fhandle& fout, 
               int r1=0, int rlen=-1);

void writeData(mdouble& y1, mdouble& y2, 
			   mdouble& y3, mdouble& y4, 
			   mdouble& y5, fhandle& fout);

void writeData(mdouble& y1, mdouble& y2, 
			   mdouble& y3, mdouble& y4, 
			   mdouble& y5, mdouble& y6, fhandle& fout);

void dumpJkSample(mdouble& y1, const int m1, const int m2, fhandle& fout);
void dumpJkSample(mdouble& y1, fhandle& fout);

#endif
