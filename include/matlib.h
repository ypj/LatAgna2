// matlib.h
#ifndef __MATLIB_H__
#define __MATLIB_H__

#include "multi1d.h"

int invertMat(multi1d<double>& varinv, int& nsvd, int& maxmode);

int checkInvMat(multi1d<double>& mat, multi1d<double>& invmat);

int get_svd_sol(mdouble& mat, mdouble& result, mdouble& sol);

int chk_svd_sol(mdouble& mat, mdouble& b_vec, mdouble& sol);

int invertCovMat(mdouble& inv, int n1, mdouble& cov, bool corr,
                 int& nsvd, int& maxmode, bool print = false);

#endif
