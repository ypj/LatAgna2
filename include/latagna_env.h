// latagna.h
#ifndef __LATAGNA_ENV_H__
#define __LATAGNA_ENV_H__

#include "fitparam.h"
#include "module-gsl/gslparam.h"

void parseOption(int argc, char** argv);
void setup(void);
void clean(void);

int n1, n2;
char fit[100];
char MINIMIZER[100];
int dof;

double TOL_FUNC_NL_MINIMIZER;
double TOL_GRAD_NL_MINIMIZER;

double TOL_AMOEBA;
int MAX_ITER_AMOEBA;
double StartFudge;

double TOL_NEWTON;
int MAX_ITER_NEWTON;

int MAX_ITER_GSL_DERIV;

double TOL_SVD_INV;
double TOL_SVD_SOL;

int nbayes;
double *c_prior;
double *inv_width;

bool guessflag;
int *c_guess;


char fullcov[100];
bool corflag;
int maxmode;

char resample[100];
int ns;
 
bool calc_meff;
bool separate_conf_file;
bool scale_factor;
bool update_corrkey;
double bc_phase;

int *twopt_idx;
int *twopt_ot;
int *twopt_opa;
int *thrpt_idx;
int *thrpt_ot;
int *thrpt_opa;

#endif
