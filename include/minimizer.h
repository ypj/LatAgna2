// minimizer.h
#ifndef __MINIMIZER_H__
#define __MINIMIZER_H__

#include "multi1d.h"

int genlin_svd(mdouble& c,
           const int n1, const int nt, const char* fit,
           double& cdf, 
           mdouble& avgtmp, mdouble& varinv);

void amoeba(mdouble& c, 
            const int n1, const int nt, const char* fit, 
            int& iter, double& chi2,
            mdouble& avgtmp, mdouble& varinv);

void newton(mdouble& c, const int n1, const int nt, const char* fit,
            int& iter, double& chi2,
            mdouble& avgtmp,  mdouble& varinv);

#endif
