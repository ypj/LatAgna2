// strutil.h
#ifndef __STRUTIL_H__
#define __STRUTIL_H__

#include <string.h>

int matchKey(const char *test, const char *keystr);
int countToken(const char *keystr, const char *delim);

#endif
