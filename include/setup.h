// setup.h
#ifndef __SETUP_H__
#define __SETUP_H__

#include "fitparam.h"

//extern double GLB_DOM_X[MAXDOM];
//extern double GLB_DOM_Y[MAXDOM];
//extern double GLB_DOM_Z[MAXDOM];

enum GLB_FIT_DOMAIN {GLB_DIR_X, GLB_DIR_Y, GLB_DIR_Z};
typedef enum GLB_FIT_DOMAIN GLB_FIT_DOMAIN;

int setFitDomain(double value, GLB_FIT_DOMAIN dir, int idx);

double getFitDomain(GLB_FIT_DOMAIN dir, int idx);

#endif
