// guess.h
#ifndef __GUESS_H__
#define __GUESS_H__

#include "multi1d.h"
#include "fitparam.h"

extern bool guessflag;

int guess(bool guessflag, mdouble& avg, mdouble& varinv,
		  const char* fit, const int n1, const int nt,
		  mdouble& cxx, int *c_guess, double *inv_width);

int guess(bool guessflag, mdouble& avg, mdouble& varinv,
          const char fit[][FITSTRLEN], const int n1[], const int nt[],
          const int npt[], const int npart, 
          mdouble& cxx, int *c_guess, double *inv_width);

#endif
