#!/bin/bash

hq=ifla
kap=0415
ml=0370

# heavy-light, heavy-heavy, smearings
buf_filekey=("WS2pt_d_d_d_d_k0.${kap}_m0.${ml}"\
             "WW2pt_d_d_k0.${kap}")

buf_corrkey=("d_d_d_d_m0.${ml}_k0.${kap}"\
             "d_d_d_d_k0.${kap}_k0.${kap}")

nbuf=${#buf_filekey[*]}


src_label=("t0_blk0" "t32_blk0")
tsrc=(0 32)

nsrc=${#tsrc[*]}


ncfg=167
cfglist=../../analy.n${ncfg}.list


PROJ=${HOME}/work/Vcb
MASSID=${hq}${kap}m${ml}
#DATADIR=${PROJ}/data/l3296f211b630m0074m037m440d/${MASSID}/   
DATADIR=${PROJ}/test-data/l3296f211b630m0074m037m440d/corr/
#prog=${HOME}/work/latagna/bin/latagna_2pt_coravg_tsrc
prog=../../bin/latagna_2pt_coravg_tsrc-knight


#(pion, rho) * momenta
ncha=22

nt=96

#----------------------------------------------------------
infile="in.2pt_coravg_tsrc_${MASSID}"
exec 7>&1
exec > $infile

echo ${nbuf} ${nsrc} ${ncfg} ${ncha} ${nt}

echo ${DATADIR%/}/

for (( ibuf=0; ibuf < ${nbuf}; ibuf++ ))
do
  echo PION_${buf_corrkey[ibuf]}_p000
  echo PION_${buf_corrkey[ibuf]}_p100
  echo PION_${buf_corrkey[ibuf]}_p110
  echo PION_${buf_corrkey[ibuf]}_p111
  echo PION_${buf_corrkey[ibuf]}_p200
  echo PION_${buf_corrkey[ibuf]}_p210
  echo PION_${buf_corrkey[ibuf]}_p211
  echo PION_${buf_corrkey[ibuf]}_p220
  echo PION_${buf_corrkey[ibuf]}_p221
  echo PION_${buf_corrkey[ibuf]}_p300
  echo PION_${buf_corrkey[ibuf]}_p310
  echo RHO_${buf_corrkey[ibuf]}_p000
  echo RHO_${buf_corrkey[ibuf]}_p100
  echo RHO_${buf_corrkey[ibuf]}_p110
  echo RHO_${buf_corrkey[ibuf]}_p111
  echo RHO_${buf_corrkey[ibuf]}_p200
  echo RHO_${buf_corrkey[ibuf]}_p210
  echo RHO_${buf_corrkey[ibuf]}_p211
  echo RHO_${buf_corrkey[ibuf]}_p220
  echo RHO_${buf_corrkey[ibuf]}_p221
  echo RHO_${buf_corrkey[ibuf]}_p300
  echo RHO_${buf_corrkey[ibuf]}_p310

  for (( isrc=0; isrc < ${nsrc}; isrc++ ))
  do
    echo ${tsrc[isrc]}
    for cfg in `cat ${cfglist}`
    do
      #echo ${buf_filekey[ibuf]}_t${src_label[isrc]}_${cfg}
      echo ${buf_filekey[ibuf]}_${src_label[isrc]}_${cfg}
    done
  done

  echo pi_${buf_corrkey[ibuf]}_p000
  echo pi_${buf_corrkey[ibuf]}_p100
  echo pi_${buf_corrkey[ibuf]}_p110
  echo pi_${buf_corrkey[ibuf]}_p111
  echo pi_${buf_corrkey[ibuf]}_p200
  echo pi_${buf_corrkey[ibuf]}_p210
  echo pi_${buf_corrkey[ibuf]}_p211
  echo pi_${buf_corrkey[ibuf]}_p220
  echo pi_${buf_corrkey[ibuf]}_p221
  echo pi_${buf_corrkey[ibuf]}_p300
  echo pi_${buf_corrkey[ibuf]}_p310
  echo rho_${buf_corrkey[ibuf]}_p000
  echo rho_${buf_corrkey[ibuf]}_p100
  echo rho_${buf_corrkey[ibuf]}_p110
  echo rho_${buf_corrkey[ibuf]}_p111
  echo rho_${buf_corrkey[ibuf]}_p200
  echo rho_${buf_corrkey[ibuf]}_p210
  echo rho_${buf_corrkey[ibuf]}_p211
  echo rho_${buf_corrkey[ibuf]}_p220
  echo rho_${buf_corrkey[ibuf]}_p221
  echo rho_${buf_corrkey[ibuf]}_p300
  echo rho_${buf_corrkey[ibuf]}_p310
done

exec 1>&7 7>&-

${prog} -f ${infile} > run.log 2>&1 < /dev/null &
