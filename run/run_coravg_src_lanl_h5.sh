#!/bin/bash

buf_filekey=("data_a06m220_src5.5_t16-20-22-24.ama.h5")

buf_corrkey=("D-421.DG5p5_1.DG5p5_1.SS")  # not used by program
                                          # but need to provide any string

buf_groupkey=("/2pt/pion/src5.5_snk5.5/pion")
buf_datakey=("AMA")

nbuf=${#buf_filekey[*]}


src_label=("")
tsrc=(0)

nsrc=${#tsrc[*]}


ncfg=650
cfglist=analy.n${ncfg}.h5.list


PROJ=${HOME}/storage1/nmefit_testdata
ID="D-421.DG5p5_1.DG5p5_1.SS"
DATADIR=${PROJ}
prog=../bin/latagna_nme_coravg_src
hdf5_shared_libs=${HOME}/work/hdf5/install/v/lib

rawformat="lanl_hdf5"

ncha=1

nt=36

#----------------------------------------------------------
infile="in.coravg_src_${ID}_h5"
exec 7>&1
exec > $infile

echo ${nbuf} ${nsrc} ${ncfg} ${ncha} ${nt}

echo ${DATADIR%/}/

for (( ibuf=0; ibuf < ${nbuf}; ibuf++ ))
do
  # param: correlator key for input file parsing
  echo ${buf_corrkey[ibuf]}

  for (( isrc=0; isrc < ${nsrc}; isrc++ ))
  do
    # param: source time shift
    echo ${tsrc[isrc]}
    # param: input raw data file name
    echo ${buf_filekey[ibuf]}${src_label[isrc]}
    if [ -f ${cfglist} ]; then
      #when there are separate hdf5 data group/set for each configuration
      for cfg in `cat ${cfglist}`
      do
        #echo ${buf_groupkey[ibuf]}/${cfg}
        echo ${buf_groupkey[ibuf]}/${cfg}/${buf_datakey[ibuf]}
      done
    else
      echo "Error: Can't find list file ${cfglist}"
      exit 2
    fi
  done

  # param: output file name for the source averaged data
  echo pion.${buf_corrkey[ibuf]}_h5.lta
done

exec 1>&7 7>&-

LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${hdf5_shared_libs}
export LD_LIBRARY_PATH

echo "Start time: `date`" > run.log

${prog} -f ${infile} --fmt ${rawformat} >> run.log 2>&1 &

wait $!
echo "End time: `date`" >> run.log
