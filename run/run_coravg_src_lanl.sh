#!/bin/bash

buf_filekey=("D-421.DG5p5_1.DG5p5_1.SS") # pion, proton

buf_corrkey=("D-421.DG5p5_1.DG5p5_1.SS")  # not used by program
                                          # but need to provide any string
nbuf=${#buf_filekey[*]}


src_label=("")
tsrc=(0)

nsrc=${#tsrc[*]}


ncfg=650
cfglist=analy.n${ncfg}.list


PROJ=${HOME}/storage1/nmefit_testdata
ID="D-421.DG5p5_1.DG5p5_1.SS"
DATADIR=${PROJ}/strip_a06m220_ama_cnf650_mom/stripped_hadspec
prog=../bin/latagna_nme_coravg_src
hdf5_shared_libs=${HOME}/work/hdf5/install/v/lib

rawformat="lanl_ascii"

ncha=1

nt=36

#----------------------------------------------------------
infile="in.coravg_src_${ID}"
exec 7>&1
exec > $infile

echo ${nbuf} ${nsrc} ${ncfg} ${ncha} ${nt}

echo ${DATADIR%/}/

for (( ibuf=0; ibuf < ${nbuf}; ibuf++ ))
do
  # param: correlator key for input file parsing
  echo PION_${buf_corrkey[ibuf]}

  for (( isrc=0; isrc < ${nsrc}; isrc++ ))
  do
    # param: source time shift
    echo ${tsrc[isrc]}
    # param: input raw data file name
    if [ -f ${cfglist} ]; then
      #when there are separate raw data files for each configuration
      for cfg in `cat ${cfglist}`
      do
        echo ${buf_filekey[ibuf]}${src_label[isrc]}.${cfg}
      done
    else
      echo pion.${buf_filekey[ibuf]}${src_label[isrc]}
    fi
  done

  # param: output file name for the source averaged data
  echo pion.${buf_corrkey[ibuf]}.lta
done

exec 1>&7 7>&-

LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${hdf5_shared_libs}
export LD_LIBRARY_PATH

echo "Start time: `date`" > run.log

${prog} -f ${infile} --fmt ${rawformat} >> run.log 2>&1 &

wait $!
echo "End time: `date`" >> run.log
