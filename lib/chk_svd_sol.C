// chk_svd_sol.C
#include <cmath>
#include "multi1d.h"
#include "error.h"
//  Check the solution vector: sol = mat^{-1} * b_vec
//     
//  - multi1d type arguments:
//  
//  mat(nlin,nlin)  : matrix
//  b_vec(nlin)     : input vector
//  sol(nlin)       : solution vector

const double TOL_SOL = 1.0e-10;

int chk_svd_sol( multi1d<double>& mat, multi1d<double>& b_vec, 
                 multi1d<double>& sol )
{
  const char* funcname = "chk_svd_sol";

  const int nlin = sol.len(0);  // number of parameters

  double b_i, t_i;
  double norm;

  // obtain t_vec[i] = (mat * sol - b_vec)[i] / b_vec[i]
  norm = 0;
  for( int i = 0; i < nlin; i++ ){
   
    b_i= b_vec.get(i);

    t_i = -b_i;
    for( int j = 0; j < nlin; j++ ) t_i += mat.get(i,j) * sol.get(j);
    t_i /= b_i;
    
    if( fabs(t_i) > norm ) norm = fabs(t_i);

  }

  // check the solution
  putMsg(funcname,"error of the solution = %13.6le\n",norm);
  
  if( norm >  TOL_SOL ){
#if 0
    errExit(700,funcname,
		"Exit due to a fatal error.\n"
		"error of the solution is greater than TOL_SOL.\n");
#else
    warnMsg(funcname,
		"error of the solution is greater than TOL_SOL (=%13.6le).\n",TOL_SOL);
#endif
  }

  return 0;
}
