// coravg_src.C
#include "latagna_env.h"
#include "global.h"
#include "io.h"
#include "error.h"
#include "strutil.h"
#include <cmath>

#include "tclap/CmdLine.h"
#include <iostream>
#include <string>

using namespace TCLAP;
using namespace std;

char infilename[256];
char rawformat[25];
char rawformat_option[25];
char t_bc[25];

int nt_max_save;

int main(int argc, char* argv[])
{
  const char *funcname = "coravg_src";

  char rawfilename[500], rawdatapath[256], filebasename[256];
  char datasetname[256];
  int ncfg, nsrc, nbuf, ncha, nt;

  double re_avg, im_avg;
  int t_shifted;
  double d_scale;

  char strtmp[256];
  int  itmp;
  double dtmp;


  // parse command line arguments
  // and setup parameters
  //-----------------------------
  parseOption(argc,argv);
  
  
  // read correlator raw data from file
  //-----------------------------------
  fhandle infile;
  fhandle rawfile;

  infile.open(infilename,"r");
 
  infile.parseLine("%d %d %d %d %d", &nbuf, &nsrc, &ncfg, &ncha, &nt);

  // max time slice for average and save
  if (nt_max_save == -1) nt_max_save = nt;  

  mint tsrc(nsrc);
  mdouble dscale(nbuf,nsrc);
  mint reim(nbuf,nsrc,2);
  mstring corrkey(nbuf,ncha);
  mdouble data(nbuf,nsrc,ncfg,ncha,nt,2);
  
  // read in data directory path
  infile.parseLine("%s", &rawdatapath);

  for(int ibuf = 0; ibuf < nbuf; ibuf++){

    // set corrkey to parse raw data file
    // corrkey = correlator_key
    corrkey.subarray(0,ibuf);
    if ( ! update_corrkey ) {
      for(int icha = 0; icha < ncha; icha++){
        infile.parseLine("%s", &strtmp);
        corrkey.set(strtmp,icha);
      }
    }

    for(int isrc = 0; isrc < nsrc; isrc++){

      // reset corrkey for parsing next source raw data file to average
      // corrkey = correlator_key
      if ( update_corrkey ) {
        infile.parseLine("%s", &strtmp);
        if ( matchKey(strtmp,"reset_corrkey") ) {
          for(int icha = 0; icha < ncha; icha++){
            infile.parseLine("%s", &strtmp);
            corrkey.set(strtmp,icha);
          }
        } 
        else if ( ! matchKey(strtmp,"keep_corrkey") ) {
          errExit(1,funcname,"expected reset_corrkey or keep_corrkey but found %s\n",strtmp);
        }
      }

      // read in source time
      infile.parseLine("%d", &itmp);
      tsrc.set(itmp,isrc);

      // set scale factor
      reim.set(0,ibuf,isrc,0);
      reim.set(1,ibuf,isrc,1);
      if ( scale_factor ) {
        infile.parseLine("%s %le", &strtmp, &dtmp);
        if ( matchKey(strtmp,"/") ) {
          dtmp = 1./dtmp;
        } 
        else if ( matchKey(strtmp,"*s") ) {
          reim.set(1,ibuf,isrc,0);
          reim.set(0,ibuf,isrc,1);
        }
        else if ( ! matchKey(strtmp,"*") ) {
          errExit(1,funcname,"scale operator must be *, /, *s \n");
        }
      } else {
        dtmp = 1.0;
      }
      //printf("isrc=%d, factor_dtmp=%le\n",isrc,dtmp);
      dscale.set(dtmp,ibuf,isrc);

      for(int icfg = 0; icfg < ncfg; icfg++){
     
        // read in raw data files
        if ( icfg == 0 || separate_conf_file ) {
          infile.parseLine("%s", &filebasename);
          strcpy(rawfilename,rawdatapath);
          strcat(rawfilename,filebasename);
          rawfile.open(rawfilename,"r",rawformat);
          if ( matchKey(rawformat,"lanl_hdf5") ) {
            rawfile.h5dread_alloc_workspace(nt);
          }
        }
        
        if ( matchKey(rawformat,"lanl_hdf5") ) {
          infile.parseLine("%s", &datasetname);
          if ( dscale.get(ibuf,isrc) != 0.0 ) rawfile.h5dopen(datasetname);
        }

        data.subarray(2,icfg);
        data.subarray(1,isrc);
        data.subarray(0,ibuf);
        if ( dscale.get(ibuf,isrc) != 0.0 ) {
          readRawData(data,rawfile,corrkey,(icfg==0 || separate_conf_file));
        }
        data.restore();
        
        if ( matchKey(rawformat,"lanl_hdf5") ) {
          if ( dscale.get(ibuf,isrc) != 0.0 ) rawfile.h5dclose();
        }
       
        if ( icfg == ncfg-1 || separate_conf_file ) {
          if ( matchKey(rawformat,"lanl_hdf5") ) {
            rawfile.h5dread_free_workspace();
          }
          rawfile.close();
        }
      
      }// for(icfg)
    }// for(isrc)
   
    // reset corrkey as an output averaged correlator file name
    for(int icha = 0; icha < ncha; icha++){
      infile.parseLine("%s", &strtmp);
      corrkey.set(strtmp,icha);
    }
    corrkey.restore();

  }// for(ibuf)
  
  infile.close();

#if 0 // garfield [debug]
  dbgMsg(funcname, "%4d %13.6le %13.6le\n",
		 7,data.get(0,0,1,3,7,0),data.get(0,0,1,3,7,1));
  dbgMsg(funcname, "%4d %13.6le %13.6le\n",
		 13,data.get(0,1,1,3,13,0),data.get(0,1,1,3,13,1));
#endif

  // src average for all (shifted) sources position with scale factor
  //------------------------------------------------------------------
  fhandle corfile;

  for(int ibuf = 0; ibuf < nbuf; ibuf++){
    for(int icha = 0; icha < ncha; icha++){

      strcpy(strtmp,(corrkey.get(ibuf,icha)).c_str());
      corfile.open(strtmp,"w");
      // garfield [FIXME] 
      // make the function corfile.open(...) accept string valued file name

      putMsg(funcname,"%s%s\n",
      "Saving the source average into the file ",corfile.name());

      corfile.put("%-16s%s\n","#   correlator:",rawdatapath);

      for(int icfg = 0; icfg < ncfg; icfg++){
        
        corfile.put("%-18s%10d\n","#   configuration",icfg+1);

        data.subarray(3,icha);
        data.subarray(2,icfg);
        data.subarray(0,ibuf);

        //for(int it = 0; it < nt; it++){
        for(int it = 0; it < nt_max_save; it++){
          re_avg = 0.;
          im_avg = 0.;
          for(int isrc = 0; isrc < nsrc; isrc++){
            t_shifted = (it+tsrc.get(isrc)+nt)%nt;
            d_scale = dscale.get(ibuf,isrc);
            if ( t_shifted < tsrc.get(isrc) ) d_scale *= bc_phase;
            re_avg += data.get(isrc,t_shifted,reim.get(ibuf,isrc,0))*d_scale;
            im_avg += data.get(isrc,t_shifted,reim.get(ibuf,isrc,1))*d_scale;
            //printf("it=%d, isrc=%d, t_shifted=%d, re=%15.8e\n",it,isrc,t_shifted,data.get(isrc,t_shifted,0));
          }
          corfile.appendData(it, re_avg/(double)nsrc, im_avg/(double)nsrc);
        }
        
        data.restore();

      }// for(icfg)

      corfile.close();

    }// for(icha)
  }// for(ibuf)

  return 0;
}


// parse command line arguments
void parseOption(int argc, char** argv)
{
  const char *funcname = "parseOption";

  try {

    // Define arguments
	//-----------------
	CmdLine cmd("Commandline options",' ',"");

	ValueArg<string> arg_input_file("f","file",
			"input file name",
			true,"","string",cmd);
	
  ValueArg<string> arg_raw_file_format("t","fmt",
		"raw file format",
		false,"default","string",cmd);
  
  ValueArg<string> arg_raw_file_format_option("","fmt-opt",
		"raw file format option",
		false,"default","string",cmd);
  
  SwitchArg arg_do_scale("s","scale",
		"scale the raw data value",
		cmd,false);
  
  SwitchArg arg_update_corrkey("","have-corrkey-synonym",
		"allow correlator key update",
		cmd,false);
	
  ValueArg<int> arg_data_dimension("","dim",
		"number of data points to be stored",
		false,-1,"int",cmd);
  
  ValueArg<string> arg_t_boundary_condition("","bc",
		"boundary condition in time",
		false,"periodic","string",cmd);
	// Parse the command line.
	cmd.parse(argc,argv);

	// Set variables
	//--------------
	strcpy(infilename,(arg_input_file.getValue()).c_str());
	strcpy(rawformat,(arg_raw_file_format.getValue()).c_str());
	strcpy(rawformat_option,(arg_raw_file_format_option.getValue()).c_str());
  scale_factor = arg_do_scale.getValue();
  update_corrkey = arg_update_corrkey.getValue();
	strcpy(t_bc,(arg_t_boundary_condition.getValue()).c_str());

	nt_max_save = arg_data_dimension.getValue();
  
  } catch ( ArgException& e )
  { cout << "ERROR: " << e.error() << " " << e.argId() << endl; }

  setup();
}


// setup from arguments
void setup()
{
  const char *funcname = "setup";

  if ( matchKey(rawformat,"default") ) { 
    separate_conf_file = true;
  } 
  else if ( matchKey(rawformat,"lanl_ascii") ) {
    separate_conf_file = false;
  }
  else if ( matchKey(rawformat,"lanl_hdf5") ) {
    separate_conf_file = false;
  }
  else{
    errExit(2,funcname,"unsupported raw file format: %s\n",rawformat);
  }
  
  if ( matchKey(rawformat_option,"sep-cfg-file") ) { 
    separate_conf_file = true;
  }
  
  if ( matchKey(t_bc,"periodic") ) { 
    bc_phase = 1.0;
  } 
  else if ( matchKey(t_bc,"antiperiodic") ) {
    bc_phase = -1.0;
  }
  else{
    errExit(2,funcname,"boundary condition must be periodic or antiperiodic: %s\n",t_bc);
  }
}
