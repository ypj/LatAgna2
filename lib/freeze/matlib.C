// matlib.C
#include "multi1d.h"
#include "error.h"
#include "fitlib.h"
#include "nr.h"
#include "prec.h"
#include "matlib.h"
#include <cstdlib>

//const int maxmode = 3;
const double TOL_MAT_INVMAT_NORM = 1.0e-10;
//const double TOL_MAT_INVMAT_NORM = 1.0e-03;

int invertMat(multi1d<double>& invmat, int& nsvd, int& maxmode)
{

  const char *funcname = "invertMat";

  int npt = invmat.len(0); // support only square matrix
 
  multi1d<double> mat(npt,npt);
  multi1d<double> u_mat(npt,npt);
  multi1d<double> diag(npt);        // W : diagonal matrix in SVD algorithm
  multi1d<double> inv_diag(npt);        // W^-1
  multi1d<double> v_mat(npt,npt);    // V : whose columns become orthonormal basis for null space after calling "svdcmp"
 
  int svderror;                    // Signular Value Decomposition error 

  double dtmp;
  double dmin, dmax, norm;

  // normalization
  norm = 0.0;  
  for (int i = 0; i < npt; i++) {
    dtmp = invmat.get(i,i);
    if (dtmp > norm) norm = dtmp;
  }

  // Initialization: copy input matrix
  for(int i = 0; i < npt; i++){
    for(int j =0 ; j <npt; j++){
      //mat.set(_round_(invmat.get(i,j),0.0001),i,j);
      //u_mat.set(_round_(invmat.get(i,j),0.0001),i,j);
      mat.set(invmat.get(i,j),i,j);
      u_mat.set(invmat.get(i,j)/norm,i,j);
    }
  }

  svderror = svdcmp(u_mat, diag, v_mat);

#if 0 // garfield [debug] check svd result
  for( int i = 0; i < npt; i++ ){
    for( int j = 0; j < npt; j++ ){
      ( j == i ) ? dtmp = diag.get(i) : dtmp = 0;
      fprintf(stdout,"[U,W,V](%2d, %2d) %13.6le %13.6le %13.6le\n",
          i,j, u_mat.get(i,j), dtmp, v_mat.get(i,j));
    }
  }
#endif
  // print singular values
  putMsg(funcname,"===== singular value : before cutoff =====\n");
  for( int i = 0; i < npt; i++ ){
    putMsg(funcname,"%13.6le, ",diag.get(i));
  }
  putMsg(funcname,"\n==========================================\n");

  if (svderror != 0) {
    errExit(svderror,funcname,"svderror occured\n");
  }

  dmax = 0.0;  // assume a positive definite matrix
  for (int i = 0; i < npt; i++) {
    dtmp = diag.get(i);
    if (dtmp > dmax) dmax = dtmp;
  }
  dmin = TOL_SVD_INV * dmax;
  nsvd = 0;
  for (int i = 0; i < npt; i++) {
    if (diag.get(i) <= dmin) {
      nsvd++;
      diag.set(-1,i);
    }
  }
  
  // print singular values
  putMsg(funcname,"===== singular value : after cutoff =====\n");
  for( int i = 0; i < npt; i++ ){
    putMsg(funcname,"%13.6le, ",diag.get(i));
  }
  putMsg(funcname,"\n=========================================\n");
  putMsg(funcname,"TOL_SVD_INV = %13.6le, min_svd = %13.6le\n",TOL_SVD_INV,dmin);
  putMsg(funcname,"=========================================\n");

  putMsg(funcname,"===== remove all but maxmode (EVs are not ordered) =====\n");
  int idel;
  int nsing = nsvd;

  if ( nsing > npt - maxmode )
    errExit(302,funcname,
        "Can't retain the required number of modes.\n"
        "-> nsing = %d , npt = %d, maxmode = %d, TOL_SVD_INV = %7.3le\n",
        nsing,npt,maxmode,TOL_SVD_INV);

  for (int i = nsing; i < npt - maxmode; i++) {
    dmin = dmax;
    idel = 0;
    for (int j = 0; j < npt; j++) {
      dtmp = diag.get(j);
      if ((dtmp > 0.0) && (dtmp <= dmin)) {
        dmin = dtmp;
        idel = j;
      }
    }
    diag.set(-1,idel);
    nsvd++;
  }
  
  putMsg(funcname,"nsing = %d, nsvd = %d, maxmode = %d\n",
         nsing,nsvd,maxmode);
  putMsg(funcname,"========================================================\n");
  
  // set deleted values to zero
  //inv_diag.zerod();

  for (int i = 0; i < npt; i++) {
    dtmp = diag.get(i) * norm;
    if (dtmp < 0.0) {
      diag.set(0.0,i);
      inv_diag.set(0.0,i);
    } else {
      inv_diag.set(1.0/dtmp,i);
    }
  }
  
  // print singular values
  putMsg(funcname,"===== singular value : inverse =====\n");
  for( int i = 0; i < npt; i++ ){
    putMsg(funcname,"%13.6le, ",inv_diag.get(i));
  }
  putMsg(funcname,"\n====================================\n");
#if 0  
  // reconstruct the normalized input matrix after appling svd cutoff
  for (int i = 0; i < npt; i++) {
    dtmp = 0.0;
    for (int k = 0; k < npt; k++) {
      dtmp += u_mat.get(i,k) * diag.get(k) * v_mat.get(i,k);
    }
    mat.set(dtmp,i,i);
  }

  for (int i = 0; i < npt; i++) {
    for (int j = 0; j < i; j++) {
      dtmp = 0.0;
      for (int k = 0; k < npt; k++) {
        dtmp += u_mat.get(i,k) * diag.get(k) * v_mat.get(j,k);
      }
      mat.set(dtmp,i,j);
      mat.set(dtmp,j,i);
    }
  }
#endif
  // construct inverse matrix
#if 0
  for (int i = 0; i < npt; i++) {
    for (int j = 0; j < npt; j++) {
      dtmp = 0.0;
      for (int k = 0; k < npt; k++) {
        dtmp += v_mat.get(i,k) * inv_diag.get(k) * u_mat.get(j,k);
      }
      invmat.set(dtmp,i,j);
    }
  }
#else // garfield [note] keep symmetric
  for (int i = 0; i < npt; i++) {
    dtmp = 0.0;
    for (int k = 0; k < npt; k++) {
      dtmp += v_mat.get(i,k) * inv_diag.get(k) * u_mat.get(i,k);
    }
    invmat.set(dtmp,i,i);
  }

  for (int i = 0; i < npt; i++) {
    for (int j = 0; j < i; j++) {
      dtmp = 0.0;
      for (int k = 0; k < npt; k++) {
        dtmp += v_mat.get(i,k) * inv_diag.get(k) * u_mat.get(j,k);
      }
      invmat.set(dtmp,i,j);
      invmat.set(dtmp,j,i);
    }
  }
#endif

#if 0 // garfield [debug]
  checkInvMat(mat,invmat);
#else 
  if ( maxmode == npt ) checkInvMat(mat,invmat);
#endif

  return 0;
}


#if 0 // garfield [update] apply tolerence to each elements

int checkInvMat(multi1d<double>& mat, multi1d<double>& invmat)
{
  // arguments:
  //  mat(npts,npts)
  //  invmat(npts,npts)  : inverse of mat

  const char *funcname = "checkInvMat";

  // npts:  Number of y data points per linear term per conf.   
  const int npts = mat.len(0);

  double norm;
  double t_ij;

  // indices:
  int i, j, k;

  // t_ij = invmat(i,k) * mat(k,j)
  // norm = abs( t_ij - I_ij )
  norm = 0;
  for( i = 0; i < npts; i++ ){
    for( j = 0; j < npts; j++ ){

      t_ij = 0;
      for( k = 0; k < npts; k++ ){
        t_ij += invmat.get(i,k) * mat.get(k,j);
      }

      if( j == i ){
        norm += abs( t_ij - 1 );
      }else{
        norm += abs( t_ij );
      }

    }
  }
  norm /= pow(npts,2);

  // check
#if 0
  if( norm >  TOL_MAT_INVMAT_NORM ){
    errExit(310,funcname,
        "FATAL!! norm is greater than TOL_MAT_INVMAT_NORM.\n"
        "-> norm = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
        norm, TOL_MAT_INVMAT_NORM);
  }else{
    dbgMsg(funcname,"norm = %7.3le\n",norm);
  }
#else
  if( norm >  TOL_MAT_INVMAT_NORM ){
    warnMsg(funcname,
        "Norm || MAT*inv(MAT) - I || is greater than tolerance.\n"
        "-> norm = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
        norm, TOL_MAT_INVMAT_NORM);
  }
#endif

  return 0;
}

#else
int checkInvMat(multi1d<double>& mat, multi1d<double>& invmat)
{
  // arguments:
  //  mat(npts,npts)
  //  invmat(npts,npts)  : inverse of mat

  const char *funcname = "checkInvMat";

  // npts:  Number of y data points per linear term per conf.   
  const int npts = mat.len(0);

  double norm, max_norm;
  double t_ij;
  mdouble t(npts,npts);

  // indices:
  int i, j, k;

  // t_ij = invmat(i,k) * mat(k,j)
  // norm = abs( t_ij - I_ij )
  norm = 0;
  max_norm = 0;
  for( i = 0; i < npts; i++ ){
    for( j = 0; j < npts; j++ ){

      t_ij = 0;
      for( k = 0; k < npts; k++ ){
        t_ij += invmat.get(i,k) * mat.get(k,j);
      }
      t.set(t_ij,i,j);

      if( j == i ){
        norm += abs( t_ij - 1 );
      }else{
        norm += abs( t_ij );
      }

      if ( norm > max_norm ) max_norm = norm;

    }
  }

  if( max_norm >  TOL_MAT_INVMAT_NORM ){
    warnMsg(funcname,
        "Norm || MAT*inv(MAT) - I || is greater than tolerance.\n"
        "-> norm = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
        max_norm, TOL_MAT_INVMAT_NORM);
#if 0 // garfield [debug]
    for( i = 0; i < npts; i++ ){
      for( j = 0; j < npts; j++ ){
        fprintf(stdout,
               "[A,Ainv,A*Ainv](%2d, %2d) %20.13le %20.13le %10.3le\n",
               i,j, mat.get(i,j), invmat.get(i,j), t.get(i,j));
      }
    }
#endif
  }

  return 0;
}


#endif
