// eff_mass.C
#include "mathlib.h"
#include "multi1d.h"
#include "fitlib.h"
#include "error.h"


double fracErr(double var, double avg)
{
  double e;
  
  (avg != 0.0) ? e = var/(avg*avg) : e = 0.0;
  return e;
}


double fracAvg(double avg1, double avg2)
{
  double r;
  
  (avg2 != 0.0) ? r = avg1/avg2 : r = 0.0;
  return r;
}


// base function is ratio() in the swmefit.
void effMass(multi1d<double>& avg, multi1d<double>& var, 
             const int n1, const int nt, const char* fit,
             multi1d<double>& rat) 
{
  const char* funcname = "effMass";

  double r, e1, e2;
  int npar, nt2;
  double indat[MAXPAR], outpar[MAXPAR], scale;
  double dtmp;

  if (strcmp(fit,"generic") != 0 &&
      strcmp(fit,"generic.v2") != 0 ) npar = NumParameters(fit);

  nt2 = nt / 2;

  if (strcmp(fit, "newt+") == 0 || strcmp(fit, "newt-") == 0
    ||strcmp(fit, "sinh+") == 0 || strcmp(fit, "sinh-") == 0) {

    e1 = fracErr(var.get(0,0),avg.get(0));
       
    scale = avg.get(1);  // this makes outpar be around 1.

    // garfield [note] check the last time slice index
    for (int t = 1; t <= (nt2 - npar); t++) {
      e2 = fracErr(var.get(t,t),avg.get(t));
      r = fracAvg(avg.get(t-1),avg.get(t));

      for (int i = 0; i < npar; ++i) 
        indat[i] = avg.get(t+i);

      // garfield [fixme] not implemented 
      //fitmes(fit, t, nt, npar, indat, scale, outpar);

      if (strcmp(fit, "newt+") == 0 || strcmp(fit, "sinh+") == 0) {
        rat.set(outpar[1],t);
      }
      else{ 
        rat.set(outpar[3],t);
      }

      //erat.set(sqrt(e1 + e2),t);
      e1 = e2;
    }
  } 
  //=======================================================================
  // garfield [2013-Sep-03]
  //
  // Calculate a definitive effective mass:
  // meff = arccosh[ {C(t+2k)+C(t-2k)} / 2*C(t) ] / (2k) , k = 1,2,...
  //
  // This definition gives the effective mass independent of its initial
  // guess. It can be used as an initial guess for all kinds of
  // (anti-)symmetric fit types, newt+/-, cosh.v2, sinh.v2, cosh, sinh.
  //=======================================================================
  else if( strcmp(fit, "generic") == 0 ){
    
    int tp, tm, trial;
    
    for (int t = 2; t <= nt-2 ; t++) {
    
      if (avg.get(t) > 0.0) {
        trial = 1;
      } else {
        rat.set(NAN,t);
        trial = -3;
        warnMsg(funcname,"%s%d%s\n",
          "Correlator has negative value. Fail to get meff[",t,"].");
      }

      while (trial>0) {

        tp = t + 2*trial;
        tm = t - 2*trial;

        if ((tp<=nt) && (tm >=0)) {
          if (trial>1) {
            warnMsg(funcname,"%s%13.6le%s\n%s%d%s%d%s%d%s\n",
              "Ratio ",r," < 1 is out of range.",
              "--> Retry using C[",tm,"], C[",t,"], C[",tp,"]");
          }

          r = fracAvg(avg.get((tp%nt))+avg.get(tm),2.0*avg.get(t));
          
          if (r >= 1.0) {
            rat.set(acosh(r)/(double)(t-tm),t);
            trial = -1;
          }else{
            trial++;
          }
        }
        else{
          //rat.set(-1.0e6,t);
          rat.set(NAN,t);
          trial = -2;
          warnMsg(funcname,"%s%d%s\n",
            "Maximum trial reached. Fail to get meff[",t,"].");
        }
      }// while
    }// for
  }
  //=======================================================================
  // garfield [2014-May-27]
  //
  // Calculate a definitive effective mass:
  // meff = ln[ C(t) / C(t+2k) ] / (2k) , k = 1,2,...
  //
  // This definition gives the effective mass independent of its initial
  // guess. It can be used as an initial guess for all kinds of
  // (anti-)symmetric fit types, newt+/-, cosh.v2, sinh.v2, cosh, sinh.
  //=======================================================================
  else if( strcmp(fit, "generic.v2") == 0 ){
    
    int tp, trial;
    
    for (int t = 0; t <= (nt/2)-2 ; t++) {
    
      if (avg.get(t) > 0.0) {
        trial = 1;
      } else {
        rat.set(NAN,t);
        trial = -3;
        warnMsg(funcname,"%s%d%s\n",
          "Correlator has negative value. Fail to get meff[",t,"].");
      }

      while (trial>0) {

        tp = t + 2*trial;

        if (tp<=nt/2) {
          if (trial>1) {
            warnMsg(funcname,"%s%13.6le%s\n%s%d%s%d%s\n",
              "Ratio ",r," < 0 is out of range.",
              "--> Retry using C[",t,"], C[",tp,"]");
          }

          r = fracAvg(avg.get(t),avg.get(tp));
          
          if (r > 0.0) {
            rat.set(log(r)/(double)(tp-t),t);
            trial = -1;
          }else{
            trial++;
          }
        }
        else{
          //rat.set(-1.0e6,t);
          rat.set(NAN,t);
          trial = -2;
          warnMsg(funcname,"%s%d%s\n",
            "Maximum trial reached. Fail to get meff[",t,"].");
        }
      }// while
    }// for

    for (int t = nt-1; t >= (nt/2)+2 ; t--) {
      // asumming symmetrized correlator
      rat.set(rat.get(nt-t),t);
    }
  }
  else {
    warnMsg(funcname,"unknown fit type\n");
  }
}
