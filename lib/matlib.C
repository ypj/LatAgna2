// matlib.C
#include "multi1d.h"
#include "error.h"
#include "fitlib.h"
#include "nr.h"
#include "prec.h"
#include "matlib.h"
#include "module-gsl/linalg_interface.h"
#include <cstdlib>

//const int maxmode = 3;
const double TOL_MAT_INVMAT_NORM = 1.0e-10;
//const double TOL_MAT_INVMAT_NORM = 1.0e-03;

int invertMat(multi1d<double>& invmat, int& nsvd, int& maxmode)
{

  const char *funcname = "invertMat";

  int npt = invmat.len(0); // support only square matrix
 
  multi1d<double> mat(npt,npt);
  multi1d<double> u_mat(npt,npt);
  multi1d<double> diag(npt);        // W : diagonal matrix in SVD algorithm
  multi1d<double> inv_diag(npt);        // W^-1
  multi1d<double> v_mat(npt,npt);    // V : whose columns become orthonormal basis for null space after calling "svdcmp"

  double dtmp, dtmp2;

  // Initialization: copy input matrix
  for(int i = 0; i < npt; i++){
    for(int j =0 ; j <npt; j++){
      dtmp = invmat.get(i,j);
      mat.set(dtmp,i,j);
      u_mat.set(dtmp,i,j);
    }
  }

  GSL_svdcmp_jacobi(u_mat, diag, v_mat, TOL_SVD_INV, nsvd, maxmode);

  putMsg(funcname,"===== singular value : inverse =====\n");
  for( int i = 0; i < npt; i++ ){
    dtmp = diag.get(i);
    if (dtmp != 0.0) dtmp = 1.0/dtmp;
    inv_diag.set(dtmp,i);
    putMsg(funcname,"%13.6le, ",dtmp);
  }
  putMsg(funcname,"\n====================================\n");
  
  // construct the (pseudo) inverse matrix
  for( int i = 0; i < npt; i++ ){
    dtmp = 0.0;
    for (int k = 0; k < npt; k++) {
      dtmp += v_mat.get(i,k) * inv_diag.get(k) * u_mat.get(i,k);
    }
    invmat.set(dtmp,i,i);
    
    for (int j = 0; j < i; j++) {
      dtmp2 = 0.0;
      for (int k = 0; k < npt; k++) {
        dtmp2 += v_mat.get(i,k) * inv_diag.get(k) * u_mat.get(j,k);
      }
      invmat.set(dtmp2,i,j);
      invmat.set(dtmp2,j,i);
    }
  }

#if 1 // garfield [debug]
  checkInvMat(mat,invmat);
#else 
  if ( maxmode == npt ) checkInvMat(mat,invmat);
#endif

  return 0;
}

#if 0
int checkInvMat(multi1d<double>& mat, multi1d<double>& invmat)
{
  // arguments:
  //  mat(npts,npts)
  //  invmat(npts,npts)  : inverse of mat

  const char *funcname = "checkInvMat";

  // npts:  Number of y data points per linear term per conf.   
  const int npts = mat.len(0);

  double norm, max_norm;
  double t_ij;
  mdouble t(npts,npts);

  // indices:
  int i, j, k;

  // t_ij = invmat(i,k) * mat(k,j)
  // norm = abs( t_ij - I_ij )
  //norm = 0;
  max_norm = 0;
  for( i = 0; i < npts; i++ ){
    for( j = 0; j < npts; j++ ){

      norm = 0;
      t_ij = 0;
      for( k = 0; k < npts; k++ ){
        t_ij += invmat.get(i,k) * mat.get(k,j);
      }
      t.set(t_ij,i,j);

      if( j == i ){
        norm += abs( t_ij - 1 );
      }else{
        norm += abs( t_ij );
      }

      if ( norm > max_norm ) max_norm = norm;

    }
  }

  if( max_norm >  TOL_MAT_INVMAT_NORM ){
    warnMsg(funcname,
        "Norm || MAT*inv(MAT) - I || is greater than tolerance.\n"
        "-> norm = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
        max_norm, TOL_MAT_INVMAT_NORM);
#if 0 // garfield [debug]
    for( i = 0; i < npts; i++ ){
      for( j = 0; j < npts; j++ ){
        fprintf(stdout,
               "[A,Ainv,A*Ainv](%2d, %2d) %20.13le %20.13le %10.3le\n",
               i,j, mat.get(i,j), invmat.get(i,j), t.get(i,j));
      }
    }
#endif
  }

  return 0;
}
#else
int checkInvMat(multi1d<double>& mat, multi1d<double>& invmat)
{
  // arguments:
  //  mat(npts,npts)
  //  invmat(npts,npts)  : inverse of mat

  const char *funcname = "checkInvMat";

  // npts:  Number of y data points per linear term per conf.   
  const int npts = mat.len(0);

  double norm, norm_ij, max_norm;
  double dtmp;
  mdouble t(npts,npts);

  // indices:
  int i, j, k;

  // t_ij = invmat(i,k) * mat(k,j)
  for( i = 0; i < npts; i++ ){
    for( j = 0; j < npts; j++ ){
      dtmp = 0;
      for( k = 0; k < npts; k++ ){
        dtmp += invmat.get(i,k) * mat.get(k,j);
      }
      t.set(dtmp,i,j);
    }
  }
  
  // | mat(i,k) * t_kj - mat(i,j) |^2
  norm = 0;
  max_norm = 0;
  for( i = 0; i < npts; i++ ){
    for( j = 0; j < npts; j++ ){
      dtmp = 0;
      for( k = 0; k < npts; k++ ){
        dtmp += mat.get(i,k) * t.get(k,j);
      }
      dtmp -= mat.get(i,j);
      norm_ij = dtmp*dtmp;
      norm += norm_ij;
      if( norm_ij > max_norm ) max_norm = norm_ij;
    }
  }
  norm /= (double)(npts*npts);

  if( max_norm >  TOL_MAT_INVMAT_NORM ){
    warnMsg(funcname,
    "Max of a norm || MAT*inv(MAT)*MAT - MAT || is greater than tolerance.\n"
    "-> max(norm) = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
    max_norm, TOL_MAT_INVMAT_NORM);
#if 0 // garfield [debug]
    for( i = 0; i < npts; i++ ){
      for( j = 0; j < npts; j++ ){
        fprintf(stdout,
               "[A,Ainv,A*Ainv](%2d, %2d) %20.13le %20.13le %10.3le\n",
               i,j, mat.get(i,j), invmat.get(i,j), t.get(i,j));
      }
    }
#endif
  }else{
    putMsg(funcname,
    "inverse check OK -> max(norm) = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
    max_norm, TOL_MAT_INVMAT_NORM);
  }
  
  if( norm >  TOL_MAT_INVMAT_NORM ){
    warnMsg(funcname,
    "Avg of a norm || MAT*inv(MAT)*MAT - MAT || is greater than tolerance.\n"
    "-> avg(norm) = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
    norm, TOL_MAT_INVMAT_NORM);
  }else{
    putMsg(funcname,
    "inverse check OK -> avg(norm) = %7.3e, TOL_MAT_INVMAT_NORM = %7.3le\n", 
    norm, TOL_MAT_INVMAT_NORM);
  }

  return 0;
}
#endif
