// get_inv_cov.C
#include "multi1d.h"
#include "error.h"
#include "matlib.h"

int invertCovMat(mdouble& inv, int n1, mdouble& cov, bool corr,
                 int& nsvd, int& maxmode, bool print)
{
  const char* funcname = "invertCovMat";

  int npt = inv.len(0);  // number of data points used in fit

  double tmp;

  // copy average and calculate inverse covariance matrix
  if (!corr) {   // Neglect correlations:
    inv.zerod(); // approximate covariance matrix by its diagonal
#if 0 
    // garfield [debug] test invertMat for the case of diagonal matrix
    for (int i = 0; i < npt; i++) { 
      inv.set(cov.get(n1+i,n1+i),i,i);
    }
    invertMat(inv, nsvd, maxmode);
#else
    for (int i = 0; i < npt; i++) { 
      inv.set(1.0/cov.get(n1+i,n1+i),i,i);
    }
    nsvd = 0;
    dbgMsg(funcname,"nsvd =%4d\n",nsvd);
    dbgMsg(funcname,"maxmode =%4d\n",maxmode);
#endif
  } else {
#if 0 // garfield [debug]
    for( int i = 0; i < npt; i++ ){
      for( int j = 0; j < i; j++ ){
        tmp = cov.get(n1+i,n1+j);
        inv.set(tmp,i,j);
        inv.set(tmp,j,i);
      }
      tmp = cov.get(n1+i,n1+i);
      inv.set(tmp,i,i);
    }
#else
    for( int i = 0; i < npt; i++ ){
      tmp = cov.get(n1+i,n1+i);
      inv.set(tmp,i,i);
      for( int j = i+1; j < npt; j++ ){
        tmp = cov.get(n1+i,n1+j);
        //printf("tmp=%lf, j=%d\n",tmp,j);
        inv.set(tmp,i,j);
        inv.set(tmp,j,i);
      }
    }
#endif
    invertMat(inv, nsvd, maxmode);
  }

  // garfield [note] need to enforce the inverse covariance matrix 
  // be a symmetric matrix?

  if( print ){
    putMsg(funcname,"covariance matrix C = [\n");
    for(int i = 0; i < npt; i++){
      for(int j = 0; j < npt; j++ ){
        if( j >= i ){
          fprintf(stdout,"%13.6le  ",cov.get(n1+i,n1+j));
        }else{
          fprintf(stdout,"%13.6le  ",cov.get(n1+i,n1+j)-cov.get(n1+j,n1+i));
        }
      }
      fprintf(stdout,"\n");
    }
    fprintf(stdout,"]\n");
    
    putMsg(funcname,"inv(C) = [\n");
    for(int i = 0; i < npt; i++){
      for(int j = 0; j < npt; j++ ){
        if( j >= i ){
          fprintf(stdout,"%13.6le  ",inv.get(i,j));
        }else{
          fprintf(stdout,"%13.6le  ",inv.get(i,j)-inv.get(j,i));
        }
      }
      fprintf(stdout,"\n");
    }
    fprintf(stdout,"]\n");
  
#if 0
    mdouble cinvc(npt,npt);

    putMsg(funcname,"C * inv(C) = [\n");
    for(int i = 0; i < npt; i++){
      for(int j = 0; j < npt; j++ ){
        tmp = 0.;
        for(int k = 0; k < npt; k++ ){
          tmp += cov.get(n1+i,n1+k)*inv.get(k,j);
        }
        cinvc.set(tmp,i,j);
        fprintf(stdout,"%13.6le  ",tmp);
      }
      fprintf(stdout,"\n");
    }
    fprintf(stdout,"]\n");
    
    putMsg(funcname,"C * inv(C) - inv(C) * C = [\n");
    for(int i = 0; i < npt; i++){
      for(int j = 0; j < npt; j++ ){
        tmp = 0.;
        for(int k = 0; k < npt; k++ ){
          tmp += inv.get(i,k)*cov.get(n1+k,n1+j);
        }
        fprintf(stdout,"%13.6le  ",cinvc.get(i,j)-tmp);
      }
      fprintf(stdout,"\n");
    }
    fprintf(stdout,"]\n");
#endif
  }

  return 0;
}
