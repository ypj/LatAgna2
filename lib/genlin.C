// genlin.C
//#include "fit.h"
#include <cmath>
#include "multi1d.h"
#include "fitlib.h"
#include "matlib.h"
#include "error.h"

extern double *c_prior;
extern double *inv_width;

//--------------------------------------------------------------
//  This subroutine solves general linear fitting
//  using minimal chi^2.
//     
//  - multi1d type arguments:
//  
//  c(nlin)          : fit parameters
//  y(npt)           : data y value
//  inv_cov(npt,npt) : inverse covariance matrix
//--------------------------------------------------------------
int genlin_svd(mdouble& c, 
           const int n1, const int nt, const char* fit, 
           double& chisq, 
           mdouble& y, mdouble& inv_cov)
{
  const char *funcname = "genlin_svd";

  // INPUTS:
  int npt = y.len(0);  // number of data points
  int nlin = c.len(0); // number of fit parameters

  int i, j, a, b;
  mdouble u(npt,nlin);   // function basis
  mdouble mat(nlin,nlin);
  mdouble sol(nlin);
  mdouble f_th(npt);
  mdouble df(npt);
  
  //double u_ai;
  //double df_a, df_b
  double inv_cov_ab;
  double chisq_L, chisq_U, chisq_D;
  double rhs, rhs2;
/* garfield [note] move this check to covfit.C
  if(nlin > npt) 
    errExit(200,funcname,"number of data points(=%d) must be larger than"
        " the number of fit parmaters(=%d).\n",npt,nlin);
*/
  setBasis(u,n1,nt,fit);
#if 0 // garfield [debug]
  for( a = 0; a < npt; a++ ){
    for( i = 0; i < nlin; i++ ){
      putMsg(funcname,"u[%d][%d]=%13.6le\n",a,i,u.get(a,i));
    }
  }
#endif

  // initialize
  c.zerod();
  mat.zerod();
  sol.zerod();
  
  // construct mat() and sol()
  //--------------------------
  for( a = 0; a < npt; a++ ){
    for( b = 0; b < npt; b++ ){

      inv_cov_ab = inv_cov.get(a,b);

      for( i = 0; i < nlin; i++ ){

        for( j = 0; j < nlin; j++ ){
          rhs = mat.get(i,j) + u.get(a,i) * inv_cov_ab * u.get(b,j);
          mat.set(rhs,i,j);
        }

        rhs2 = sol.get(i) + u.get(a,i) * inv_cov_ab * y.get(b);
        sol.set(rhs2,i);

      }
    }
  }
    
/* It gives the same results. Only the loop order is different.    
  for( i = 0; i < nlin; i++ ){
    rhs2 = 0;
    for( j = 0; j < nlin; j++ ){
      rhs  = 0;
      for( a = 0; a < npt; a++ ){
        u_ai = u.get(a,i);
        for( b = 0; b < npt; b++ ){

          rhs  += u_ai * inv_cov.get(a,b) * u.get(b,j);

          if( j == 0 ){ // make rhs2 do not be affected by sum over j
            rhs2 += u_ai * inv_cov.get(a,b) * y.get(b);
          }

        }// for(b);
      }// for(a)
      mat.set(rhs,i,j);
    }// for(j)
    sol.set(rhs2,i);
  }// for(i)
*/
  
  // Modification by the Bayesian prior
  double norm = 0.0;  
  for( i = 0; i < nlin; i++ ){
    rhs = mat.get(i,i) + pow(inv_width[i],2);
    mat.set(rhs,i,i);
    if (rhs > norm) norm = rhs;
    
    rhs = sol.get(i) + c_prior[i]*pow(inv_width[i],2);
    sol.set(rhs,i);
  }
  
  // normalization
  for( i = 0; i < nlin; i++ ){
    for( j = 0; j < nlin; j++ ){
      mat.set(mat.get(i,j)/norm,i,j);
    }
    sol.set(sol.get(i)/norm,i);
  }


  // SOLVE: mat(i,j) * c(j) = sol(i), using SVD algorithm
  //------------------------------------------------------

  get_svd_sol(mat,c,sol);

  // obtain chi^2
  for( a = 0; a < npt; a++ ){
    rhs = 0.0;
    for( i = 0; i < nlin; i++ ){
      rhs += c.get(i) * u.get(a,i);
    }
    f_th.set(rhs,a);
    df.set( y.get(a) - rhs, a );  // rhs = f_th(a)
  }

  chisq_L  = 0.0;
  for( a = 0; a < npt; a++ ){
    for( b = 0; b < a; b++ ){
      chisq_L += df.get(a) * inv_cov.get(a,b) * df.get(b);
    }
  }
  
  chisq_U  = 0.0;
  for( a = 0; a < npt; a++ ){
    for( b = a+1; b < npt; b++ ){
      chisq_U += df.get(a) * inv_cov.get(a,b) * df.get(b);
    }
  }
  
  chisq_D  = 0.0;
  for( a = 0; a < npt; a++ ){
    chisq_D += df.get(a) * inv_cov.get(a,a) * df.get(a);
  }
  
  chisq = chisq_L + chisq_U + chisq_D;
  
  // Modification by the Bayesian prior
  for( i = 0; i < nlin; i++ ){
    chisq += pow(inv_width[i]*(c.get(i)-c_prior[i]),2);
  }

  //putMsg(funcname,"chisq_L = %13.6le\n",chisq_L);
  //putMsg(funcname,"chisq_U - chisq_L = %13.6le\n",chisq_U-chisq_L);
  //putMsg(funcname,"chisq_D = %13.6le\n",chisq_D);
  //putMsg(funcname,"chisq = chisq_L + chisq_U + chisq_D + chisq_prior = %13.6le\n",chisq);
  
//  double tmp;
//  chisq  = 0.0;
//  for( a = 0; a < npt; a++ ){
//    df_a = df.get(a);
//    for( b = 0; b < npt; b++ ){
//      df_b = df.get(b);
//      chisq += df_a * inv_cov.get(a,b) * df_b;
//    }
//  }

  return 0;
}
