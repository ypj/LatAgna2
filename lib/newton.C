// newton.C
#include "global.h"
#include "multi1d.h"
#include "fitlib.h"
#include "nr.h"
#include "minimizer.h"
#include "error.h"
#include <cmath>
#include <cstdlib>

//int    ncom;
//double *pcom;
//double *xicom;
//
//double avgtmpcp[MAXNT];
//double varinvcp[MAXNT][MAXNT];
//int    nptcp;


///////////////////////////////////////////////////////////////////
//    Minimizer using Newton method
///////////////////////////////////////////////////////////////////
//const int MAX_ITER_NEWTON = 2500;
extern int MAX_ITER_NEWTON;
//const double TOL_NEWTON = 1.0e-10;
extern double TOL_NEWTON;

void newton(multi1d<double>& c, const int n1, const int nt, const char* fit,
			int& iter, double& chi2,
			multi1d<double>& avgtmp,  multi1d<double>& varinv)
{
  const char* funcname = "newton";

  int ndim = c.len(0);
  int npt = avgtmp.len(0);

  multi1d<double> A(ndim,ndim); //Hessian matrix
  multi1d<double> b(ndim);      //Negative gradient vector
  multi1d<double> d(ndim);      //Direction vector
  double chi_sq_old = 0.0;
  double chi_sq_new = 0.0;
  double delta_chi_sq, delta_c;
    
  multi1d<double> u(ndim);      //Normalization parm. of c
  
  //Linmin
  multi1d<double> xi(ndim);
  double fret;
  
  //SVD
  multi1d<double> diag(ndim);
  multi1d<double> vtemp(ndim,ndim);
  
  int i, j, k;
  const double TINY = 1.0e-30;

  double dtmp, dtmp2;

  //Main Loop
  for(iter = 1; iter <= MAX_ITER_NEWTON; iter++)
  {
    // u(i) is renormalization parameter for c(i)
    // every c(i) is replaced by c(i)/u(i) and
    // every dChiSq and ddChiSq is multiplied by u(i) or u(i)*u(j)
    // if c(i) is 0, do not renormalize it
    for(i=0; i<ndim; i++) {
	  dtmp = c.get(i);
      (dtmp == 0) ? u.set(1.0,i) : u.set(fabs(dtmp),i);
    }
    
    // Calculate direction vector, d = A_inv b
	//----------------------------------------
    
    // Calculate hessian matrix A
    for(i=0; i<ndim; i++) {
      for(j=i; j<ndim; j++) {
        dtmp = ddChiSquared(c,n1,nt,fit,avgtmp,varinv,i,j)
		       *u.get(i)*u.get(j);

        if(dtmp < TINY) {
		  A.set(0.0,i,j);
		  A.set(0.0,j,i);
        }else{
		  A.set(dtmp,i,j);
		  A.set(dtmp,j,i);
		}
      }
    }

    // Calculate negative gradient vector b
    for(i=0; i<ndim; i++){
      b.set(-dChiSquared(c,n1,nt,fit,avgtmp,varinv,i)*u.get(i),i); 
    }
  
    //Singular Value decomposition of matrix A to solve Ad = b
#if 0
	dsvdcmp(A,ndim,ndim,diag,vtemp);
#else
	svdcmp(A,diag,vtemp);
#endif
    
    //Calculate 1/diag. If diag(i) is zero, set 1/diag(i) to zero
    for(i=0; i<ndim; i++) {
	  dtmp = diag.get(i);
      if(fabs(dtmp) < TINY) {
		//warnMsg(funcname,"SVD found too small(zero) eigenvalue!\n");
        diag.set(0.0,i);
      }
      else {
        diag.set(1.0/dtmp,i);
      }
    }
   
	//garfield [CKPOINT]
    
    //Calculate d = A_inv b
    for(i=0; i<ndim; i++) {
	  dtmp = 0.0;
      for(j=0; j<ndim; j++) {
		dtmp2 = vtemp.get(i,j)*diag.get(j);
        for(k=0; k<ndim; k++) {
          dtmp += dtmp2*A.get(k,j)*b.get(k);
        }
      }
	  d.set(dtmp,i);
    }
    
    // Find minimum point along the direction d(j) using linmin_dp
	//------------------------------------------------------------
    for(i=0; i<ndim; i++) {
      xi.set(d.get(i)*u.get(i),i);
    }

#if 0 // garfield [note] why is this needed?
	nptcp = npt;
    
    for(i=0; i<npt; i++) {
      avgtmpcp.set(avgtmp.get(i),i);
      for(j=0; j<npt; j++) {
        varinvcp.set(varinv.get(i,j),i,j);
      }
    }
#endif

    //xi is the direction vector, and fret is the new chisq after linmin    
    linmin(c,n1,nt,fit,avgtmp,varinv,xi,&fret);
    //Returned xi is the displacement vector
    //for the unnormalized parameter vector c

    chi_sq_old = chi_sq_new;
    chi_sq_new = fret;
	//chi_sq_old = chi2;
	//chi2 = chi_sq_new = fret;
    delta_chi_sq = 2.0*fabs((chi_sq_old - chi_sq_new)/(chi_sq_old + chi_sq_new+TINY));

    //Calculate delta_c = |xi/u|
    delta_c = 0.;
    for(i=0; i<ndim; i++) {
      delta_c += pow(xi.get(i)/u.get(i),2.0);
    }
    delta_c = sqrt(delta_c);

    //Exit the main loop if the update of chisq is small and gradient is small
    if((iter > 1) && 
	   (delta_chi_sq <= TOL_NEWTON) && (delta_c <= TOL_NEWTON)) {
      chi2 = chi_sq_new;
      return;
    }

#if 0 // garfield [debug]
	putMsg(funcname,"newton iteration = %d\n",iter);
	fflush(stdout);
#endif

  }// End of the main loop

  //If it reaches Maximum iteration, returns -Maxit
  iter = -MAX_ITER_NEWTON;
  chi2 = chi_sq_new;
}
