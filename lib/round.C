// round.C
#include <cmath>

double _round_(double x,double prec)
{
  int n;
  double f;

  n = static_cast<int> (log10(x));
  f = x*pow(10.0,-n);
  return (double) (floor(f*(1.0f/prec) + 0.5)/(1.0f/prec))*pow(10.0,n);
}
