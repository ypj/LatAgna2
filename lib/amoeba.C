// amoeba.C
#include "fitlib.h"
#include "multi1d.h"
#include "error.h"
#include <cmath>
#include <cstdlib>

//const int MAX_ITER_AMOEBA = 9999;
extern int MAX_ITER_AMOEBA;
//const double TOL_AMOEBA = 1.0e-10;
//const double TOL_AMOEBA = 1.0e-6;
//const double TOL_AMOEBA = 3.3e-3;
extern double TOL_AMOEBA;
extern double StartFudge;

void amoeba(multi1d<double>& c, 
			const int n1, const int nt, const char* fit, 
			int& iter, double& chi2,
			multi1d<double>& avgtmp, multi1d<double>& varinv)
{
  const char *funcname = "amoeba";

  int ndim = c.len(0);
  int mpts = ndim+1; // the number of points in the simplex
  int npt = avgtmp.len(0);

  const double ALP = 1.0; // averaging factor
  const double BET = 0.5; // contraction factor 
  const double GAM = 2.0; // expansion factor
  const double TINY = 1.0e-16;

  multi1d<double> pr(ndim), prr(ndim), pbar(ndim);
  double rtol, ypr, yprr;
  double dtmp, dtmp2;

  int ihi;  // the point which has the highest f_value
  int inhi; // the point which has the next highest f_value
  int ilo;  // the point which has the lowest f_value

  multi1d<double> p(ndim+1,ndim);
  p.zerod();

  multi1d<double> f(npt);                
  f.zerod();        
  
  multi1d<double> y(mpts); // value of chi-square at certain vertex

  // Initialize
  //-----------
  //double StartFudge = 1.5;
  // Fill in initial simplex of points in the "npa" parameter space.
  // Assume 50% errors on previous/guessed parameters, c. 
  // If the parameters c have not been set, define a simplex from the
  // point (1,1,...,1).
  
  for(int i = 0; i < mpts; i++){
	for(int j = 0; j < ndim; j++){
	   dtmp = c.get(j);
	   (abs(dtmp) > TINY) ? p.set(dtmp,i,j) : p.set(1.0,i,j);
	}
	if( i != ndim ) p.set(p.get(i,i)*StartFudge,i,i);

	// Obtain fitting function values at the i-th vertex
	p.subarray(0,i);
	//f_value(p,f,n1,nt,fit);
	//p.restore();
	
	// Calculate the initial Chi^2 for an i-th vertex
	//y.set(ChiSquared(avgtmp,f,varinv),i);
	y.set(ChiSquared(p,n1,nt,fit,avgtmp,varinv),i);
	p.restore();
  }
  
  // Iterations begin
  //-----------------
  iter = 0; 

  for (;;) {

    // Find the highest, next-highest, and lowest points
    //--------------------------------------------------
    ilo = 0;
    if (y.get(0) > y.get(1)) {
      ihi = 0;
      inhi = 1;
    } else {
      ihi = 1;
      inhi = 0;
    }
    for (int i = 0; i < mpts; ++i) {
	  dtmp = y.get(i);
      if (dtmp < y.get(ilo)) ilo = i;
      if (dtmp > y.get(ihi)) {
        inhi = ihi;
        ihi = i;
      } 
      else if ( (dtmp > y.get(inhi)) && (i != ihi) ) inhi = i;
    }

    // Compute the fractional range from highest to lowest
    // and finalize if satisfactory
    //-----------------------------------------------------
    dtmp = y.get(ihi);
	dtmp2 = y.get(ilo);
	rtol = 2.0 * fabs(dtmp-dtmp2) / (fabs(dtmp)+fabs(dtmp2)+TINY);
    if (rtol < TOL_AMOEBA) goto finalize;

    // If # of iterations exceed the max iter number,
    // then return -1 times max iter number
    //-------------------------------------------------
    if (iter >= MAX_ITER_AMOEBA) {
      iter = -MAX_ITER_AMOEBA;
      goto finalize;
    }
  
#if 0 // garfield [debug]
	putMsg(funcname,"amoeba iteration = %d\n",iter);
	fflush(stdout);
#endif
	iter += 2;
    
    // Calculate the center ponint of the vertices 
	// excluding the highest point.
    // With this point we can calculate trial points.
    //-----------------------------------------------
    for (int j = 0; j < ndim; j++) {
	  dtmp = 0.0;
	  for (int i = 0; i < mpts; i++) {
		if (i != ihi) dtmp += p.get(i,j);
	  }
	  pbar.set(dtmp/(double)ndim,j);
	}

    // Obtain the reflected point from the highest point
    //
    //                               == Reflection ==
    //---------------------------------------------------
    for (int j = 0; j < ndim; j++) {
      pr.set((1.0 + ALP)*pbar.get(j) - ALP*p.get(ihi,j), j);
    }

    //f_value(pr,f,n1,nt,fit);

    //ypr = ChiSquared(avgtmp,f,varinv);
    ypr = ChiSquared(pr,n1,nt,fit,avgtmp,varinv);

    // If the f_value of the reflected point is smaller than 
    // that of the lowest point
    //
    //                          == If reflection is good ==
    //-------------------------------------------------------
    if (ypr <= y.get(ilo)) {
      // Expand the reflected point along the line connecting 
      // the center point to the reflected point by a factor 2
      //
      //                                    == Expansion ==
      //-------------------------------------------------------
      for (int j = 0; j < ndim; j++) 
        prr.set(GAM*pr.get(j) + (1.0 - GAM)*pbar.get(j), j);
      
      //f_value(prr,f,n1,nt,fit);
      
      //yprr = ChiSquared(avgtmp,f,varinv);
      yprr = ChiSquared(prr,n1,nt,fit,avgtmp,varinv);

      // If the f_value of the expanded point is smaller than
      // that of the lowest point, then 
      //------------------------------------------------------
      if (yprr < y.get(ilo)) {
        // the highest point is replaced by the expanded point 
        for (int j = 0; j < ndim; j++) {
          p.set(prr.get(j),ihi,j);
        }
        y.set(yprr,ihi);
      } 
      else {
        // the highest point is replaced by the reflected point
        for (int j = 0; j < ndim; j++) {
          p.set(pr.get(j),ihi,j);
        }
        y.set(ypr,ihi);
      }
    } 
    // If the f_value of the reflected point is larger than 
    // that of the next highest point
    //
    //                    == If reflection is not good ==
    //-------------------------------------------------------
    else if (ypr >= y.get(inhi)) {
      // If the f_value of the reflected point is smaller than 
      // that of the highest point
      //-------------------------------------------------------
      if (ypr < y.get(ihi)) {
        // the highest point is replaced by the reflected point
        for (int j = 0; j < ndim; j++) {
          p.set(pr.get(j),ihi,j);
        }
        y.set(ypr,ihi);
      }

      // Contract the highest point along the line connecting 
      // the highest point to the center point by a factor 1/2 
      //
      //                                   == Contraction ==
      //-------------------------------------------------------
      for (int j = 0; j < ndim; j++) {
        prr.set(BET*p.get(ihi,j) + (1.0 - BET)*pbar.get(j), j);
      }

      //f_value(prr,f,n1,nt,fit);
      
      //yprr = ChiSquared(avgtmp,f,varinv);
      yprr = ChiSquared(prr,n1,nt,fit,avgtmp,varinv);

      // If the f_value of the contracted point is smaller than
      // that of the highest point 
      //
      //                       == If contraction is working ==
      //--------------------------------------------------------
      if (yprr < y.get(ihi)) {
        // the highest point is replaced by the contracted point
        for (int j = 0; j < ndim; j++) {
          p.set(prr.get(j),ihi,j);
        }
        y.set(yprr,ihi);
      } 
      else {
		// both of the reflection and contraction are not working

        // Contraction around the lowest point
        //
        //         == multiple contraction ==
        //-------------------------------------
        for (int i = 0; i < mpts; i++) {
          if (i != ilo) {
            for (int j = 0; j < ndim; j++) {
              pr.set(0.5*(p.get(i,j) + p.get(ilo,j)),j);
              p.set(pr.get(j),i,j);
            }
            //f_value(pr,f,n1,nt,fit);
            //y.set(ChiSquared(avgtmp,f,varinv),i);
            y.set(ChiSquared(pr,n1,nt,fit,avgtmp,varinv),i);
          }
        }
		iter += ndim;
      }
    } 
    else {
	  // Just replace the highest point with the reflected point
	  //                    
	  //              == reflection is neither good nor bad ==
	  //---------------------------------------------------------
      for (int j = 0; j < ndim; j++){
		p.set(pr.get(j),ihi,j);
	  }
      y.set(ypr,ihi);

	  --iter;
    }
  } // infinite for loop

finalize:
#if 1
  // Average of all the vertices in the simplex
  for(int j = 0; j < ndim; j++){
    dtmp = 0.0;
    for(int i = 0; i < mpts; i++) dtmp += p.get(i,j);
    c.set(dtmp/(double)(mpts),j);  
  }

  // Average the minimal Chi^2
  chi2 = 0.0;
  for(int i = 0; i < mpts; i++) chi2 += y.get(i);
  chi2 /= (double)(mpts);
#else 
  // garfield [replace]
  // Take the lowest simplex point as the best point.
  // This is what the Numerical Recipes does.
  for(int j = 0; j < ndim; j++){
	c.set(p.get(ilo,j),j);  
  }

  chi2 = y.get(ilo);
#endif
  return;
}
