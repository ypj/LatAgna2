// write_data.C
#include "io.h"
#include "error.h"
#include <cmath>

void writeData(mdouble& y1, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i);

	if( isnan(colA) )
	  fout.put("#");

	fout.appendData(i,colA);
  }
}


void writeData(mdouble& y1, mdouble& y2, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA, colB;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i);
	colB = y2.get(i);

	if( isnan(colA) || isnan(colB) )
	  fout.put("#");

	fout.appendData(i,colA,colB);
  }
}


void writeData(mint& x, mdouble& y1, mdouble& y2, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA, colB;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i);
	colB = y2.get(i);

	if( isnan(colA) || isnan(colB) )
	  fout.put("#");

	fout.appendData(x.get(i),colA,colB);
  }
}


void writeData(mdouble& y1, mdouble& y2,
			   mdouble& y3, mdouble& y4, fhandle& fout, int r1, int rlen)
{
  const char *funcname="writeData4";
  int nrow;
  
  if ( rlen == -1 ) { // default
    nrow = y1.len(0);
  } else {
    if (rlen <=0 ) 
      errExit(1,funcname,"data length must be positive but %d\n",rlen);
    nrow = rlen;
  }
  
  double colA, colB, colC, colD;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i+r1);
	colB = y2.get(i+r1);
	colC = y3.get(i+r1);
	colD = y4.get(i+r1);

	if( isnan(colA) || isnan(colB) || isnan(colC) || isnan(colD) )
	  fout.put("#");

	fout.appendData(i,colA,colB,colC,colD);
  }
}


void writeData(mdouble& y1, mdouble& y2,
			   mdouble& y3, mdouble& y4,
			   mdouble& y5, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA, colB, colC, colD, colE;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i);
	colB = y2.get(i);
	colC = y3.get(i);
	colD = y4.get(i);
	colE = y5.get(i);

	if( isnan(colA) || isnan(colB) || isnan(colC) || isnan(colD) ||
		isnan(colE) )
	  fout.put("#");

	fout.appendData(i,colA,colB,colC,colD,colE);
  }
}


void writeData(mdouble& y1, mdouble& y2,
			   mdouble& y3, mdouble& y4,
			   mdouble& y5, mdouble& y6, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA, colB, colC, colD, colE, colF;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i);
	colB = y2.get(i);
	colC = y3.get(i);
	colD = y4.get(i);
	colE = y5.get(i);
	colF = y6.get(i);

	if( isnan(colA) || isnan(colB) || isnan(colC) || isnan(colD) ||
		isnan(colE) || isnan(colF) )
	  fout.put("#");

	fout.appendData(i,colA,colB,colC,colD,colE,colF);
  }
}


void dumpJkSample(mdouble& y1, const int m1, const int m2, fhandle& fout)
{
  int nrow = y1.len(0);
  
  double colA, colB;

  for(int i = 0; i < nrow; i++)
  {
	colA = y1.get(i,m1);
	colB = y1.get(i,m2);

	fout.put("#");

	fout.appendData(i,colA,colB);
  }
}


void dumpJkSample(mdouble& y1, fhandle& fout)
{
  const char *funcname = "dumpJkSample";
  int nrow = y1.len(0);
  int ncol = y1.len(1);
  int nanflag = 0;
  double dtmp;
  
  double *col = (double *) malloc(sizeof(double)*ncol);
  if (col==NULL) errExit(1,funcname,"malloc failed\n");

  for(int i = 0; i < nrow; i++)
  {
    for(int j = 0; j < ncol; j++) {
      dtmp = y1.get(i,j);
      col[j] = dtmp;
      if( isnan(dtmp) ) nanflag = 1;
    }

    if( nanflag ) fout.put("#");

	fout.appendData(i,col,ncol);
  }

  free(col);
}
