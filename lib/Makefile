#---definitions---------------------------------------------------
# This section contains customizable variables.
BIN := ../bin
VSTR := _pme

# !!! END OF CUSTOMIZABLE SECTION                !!!
# !!! It is not recommended to modify the below. !!!
#=================================================================

MAKEFILE := Makefile

TSTAMP := $(shell date +%s)

#LOGFILE := compile-$(TSTAMP).log

#TARGET := latagna_2pt_corfit
LNAME := latagna_nme
FIT1 := corfit
FIT2 := dispfit
AVG3 := coravg_src
RAT5 := ratio_nme
RAT6 := cor_ratio
RAT7 := ratio_sum_nme
BIN8 := cormath
RAT9 := ratio_dbl
BIN10 := meff

DFLAG := -DENABLE_DEBUG_NO\
		 -DVERBOSE_NO\
		 -DSEP_TWOPT_AMP
#        -DUSE_IMAGINARY_PART
#-----------------------------------------------------------------
CC := g++
CFLAGS = -std=c++11 -O3 -fPIC -Wall\
		 -Wno-write-strings\
		 -Wno-unused-variable\
		 -Wno-unused-but-set-variable\
		 -Wno-array-bounds\
		 $(DFLAG)
#CFLAGS = -std=c++11 -pg -O3 -fPIC -Wall -fopenmp\
#		 -Wno-write-strings\
#		 -Wno-unused-variable\
#		 -Wno-unused-but-set-variable\
#		 -Wno-array-bounds\
#		 $(DFLAG)

INC := -I${HOME}/work/latagna_nme/include
LIB := -L${HOME}/work/latagna_nme/lib/module-gsl

GSL := ${HOME}/work/gsl/install/v
HDF5 := ${HOME}/work/hdf5/install/v
INC += -I$(GSL)/include
INC += -I$(HDF5)/include
LIB += -L$(GSL)/lib
LIB += -L$(HDF5)/lib

#-----------------------------------------------------------------

HDR := $(wildcard $(INC)/*.h)

SRC := $(wildcard *.C)

OBJA := $(SRC:%.C=%.o)
OBJ1 = $(filter-out $(FIT1).o,$(OBJA))
OBJ2 = $(filter-out $(FIT2).o,$(OBJ1))
OBJ3 = $(filter-out $(AVG3).o,$(OBJ2))
OBJ5 = $(filter-out $(RAT5).o,$(OBJ3))
OBJ6 = $(filter-out $(RAT6).o,$(OBJ5))
OBJ7 = $(filter-out $(RAT7).o,$(OBJ6))
OBJ8 = $(filter-out $(BIN8).o,$(OBJ7))
OBJ9 = $(filter-out $(RAT9).o,$(OBJ8))
OBJ10 = $(filter-out $(BIN10).o,$(OBJ9))
OBJ = $(OBJ10)
#-----------------------------------------------------------------
help :
	@echo "usage: make [OPTIONS]";\
	echo "[OPTIONS]";\
	echo "help        : print this usage (Default)";\
	echo "clean       : remove output files *.o, *~, core* ";\
	echo "all         : all executables";\
	echo "$(FIT1)     : general 2- and 3-point correlator fitter";\
	echo "$(FIT2)     : dispersion relation fitter";\
	echo "$(AVG3)     : correlator source average";\
	echo "$(RAT5)     : nme 2pt and 3pt ratio constructor from fit results";\
	echo "$(RAT6)     : combine correlator files to form a ratio";\
	echo "$(RAT7)     : nme 2pt and 3pt ratio and sum of ratio constructor from fit results";\
	echo "$(BIN8)     : correlator math preprocessor";\
	echo "$(RAT9)     : 3pt double ratio constructor from fit results";\
	echo "$(BIN10)    : effective mass for any correlator";\

all : $(FIT1) $(FIT2) $(AVG3) $(RAT5) $(RAT6) $(RAT7) $(BIN8) $(RAT9) $(BIN10) $(MAKEFILE) 

$(FIT1) : modgsl.a $(FIT1).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(FIT1).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(FIT1)_avgmeff : modgsl.a $(FIT1)_avgmeff.vo $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) -DAVGMEFF $(LIB) -o $(LNAME)_$@ $(FIT1)_avgmeff.vo $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(FIT2) : modgsl.a $(FIT2).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(FIT2).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(AVG3) : modgsl.a $(AVG3).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(AVG3).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(RAT5) : modgsl.a $(RAT5).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(RAT5).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(RAT6) : modgsl.a $(RAT6).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(RAT6).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(RAT7) : modgsl.a $(RAT7).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(RAT7).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(BIN8) : modgsl.a $(BIN8).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(BIN8).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(RAT9) : modgsl.a $(RAT9).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(RAT9).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(BIN10) : modgsl.a $(BIN10).o $(OBJ) $(MAKEFILE)
	$(CC) $(CFLAGS) $(LIB) -o $(LNAME)_$@ $(BIN10).o $(OBJ) -lmodgsl -lgsl -lgslcblas -lm -lhdf5
	mv $(LNAME)_$@ $(BIN)/$(LNAME)_$@$(VSTR)

$(FIT1)_avgmeff.vo : $(FIT1).C $(MAKEFILE)
	$(CC) $(CFLAGS) -DAVGMEFF $(INC) -c $< -o $@

modgsl.a :
	make -C module-gsl

%.o : %.C $(MAKEFILE)
	$(CC) $(CFLAGS) $(INC) -c $<

clean :
	rm -f *.o *~ core*

.PHONY : all clean modgsl.a
