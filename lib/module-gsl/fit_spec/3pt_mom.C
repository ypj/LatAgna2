double threePointMom(const double *c, int i, const int tsep, 
                     const char *fit, const int par) 
{
  // garfield [fixme] 
  // assume 2+0.3pt.mom or 3+0.3pt.mom
  const char *funcname = "threePointMom";
 
  double tt, tsym;
  double f = 0;
  double fM[3], fE[3];
  double cM[3][2], cE[3][2];
  int state[2];
  int issym = 0;

  tt = i-tsep;
  tsym = -i;
  
  GSL_numStates(fit,state,&issym);
  
  if ( state[0] == 2 ) {
  
    cM[0][1] = c[1];
    cM[1][1] = c[3];
    
    cE[0][1] = c[5];
    cE[1][1] = c[7];

#ifdef SEP_TWOPT_AMP
    // c[8] = |A0|^2 at p=0
    // c[9] = |A1|^2/|A0|^2 at p=0
    // c[10] = |A0|^2 at p!=0
    // c[11] = |A1|^2/|A0|^2 at p!=0
    cM[0][0] = sqrt(c[8]); 
    cM[1][0] = sqrt(c[9]); 
    
    cE[0][0] = sqrt(c[10]); 
    cE[1][0] = sqrt(c[11]); 
#else
    cM[0][0] = 1.0; 
    cM[1][0] = 1.0; 
    
    cE[0][0] = 1.0; 
    cE[1][0] = 1.0; 
#endif

    for ( int i=0; i<state[0]; i++) {
      fM[i] = groundState(cM[i],tt);
      fE[i] = groundState(cE[i],tsym);
    }

    f = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
    f *= c[0]*fM[0]*fE[0];
  }
  else if ( state[0] == 3 ) {
    
    cM[0][1] = c[1];
    cM[1][1] = c[3];
    cM[2][1] = c[3]+c[9];
    
    cE[0][1] = c[5];
    cE[1][1] = c[7];
    cE[2][1] = c[7]+c[11];

#ifdef SEP_TWOPT_AMP
    // c[15] = |A0|^2 at p=0
    // c[16] = |A1|^2/|A0|^2 at p=0
    // c[17] = |A2|^2/|A0|^2 at p=0
    // c[18] = |A0|^2 at p!=0
    // c[19] = |A1|^2/|A0|^2 at p!=0
    // c[20] = |A2|^2/|A0|^2 at p!=0
    cM[0][0] = sqrt(c[15]); 
    cM[1][0] = sqrt(c[16]); 
    cM[2][0] = sqrt(c[17]); 
    
    cE[0][0] = sqrt(c[18]); 
    cE[1][0] = sqrt(c[19]); 
    cE[2][0] = sqrt(c[20]); 
#else
    cM[0][0] = 1.0; 
    cM[1][0] = 1.0; 
    cM[2][0] = 1.0; 
    
    cE[0][0] = 1.0; 
    cE[1][0] = 1.0; 
    cE[2][0] = 1.0; 
#endif
    
    for ( int i=0; i<state[0]; i++) {
      fM[i] = groundState(cM[i],tt);
      fE[i] = groundState(cE[i],tsym);
    }

    f = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
            + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
            + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
    f *= c[0]*fM[0]*fE[0];
  } else {
    errExit(101,funcname,
        "support up to 3 states but try %d states.\n",state[0]);
  }

  return f;
}


double da_dthreePointMom(const double *c, int a, int i, const int tsep, 
                         const char *fit, const int par) 
{
  // garfield [fixme] 
  // assume 2+0.3pt.mom or 3+0.3pt.mom
  const char *funcname = "da_dthreePointMom";
  
  double tt, tsym;
  double df = 0;
  double fM[3], fE[3];
  double cM[3][2], cE[3][2];
  int state[2];
  int issym = 0;

  tt = i-tsep;
  tsym = -i;
  
  GSL_numStates(fit,state,&issym);
  
  if ( state[0] == 2 ) {
  
    cM[0][1] = c[1];
    cM[1][1] = c[3];
    
    cE[0][1] = c[5];
    cE[1][1] = c[7];

#ifdef SEP_TWOPT_AMP
    // c[8] = |A0|^2 at p=0
    // c[9] = |A1|^2/|A0|^2 at p=0
    // c[10] = |A0|^2 at p!=0
    // c[11] = |A1|^2/|A0|^2 at p!=0
    cM[0][0] = sqrt(c[8]); 
    cM[1][0] = sqrt(c[9]); 
    
    cE[0][0] = sqrt(c[10]); 
    cE[1][0] = sqrt(c[11]); 
#else
    cM[0][0] = 1.0; 
    cM[1][0] = 1.0; 
    
    cE[0][0] = 1.0; 
    cE[1][0] = 1.0; 
#endif

    for ( int i=0; i<state[0]; i++) {
      fM[i] = groundState(cM[i],tt);
      fE[i] = groundState(cE[i],tsym);
    }

    //f = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
    //f *= c[0]*fM[0]*fE[0];
    
    if (a == 0) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
      df *= fM[0]*fE[0];
    } 
    else if (a == 1) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
      df *= c[0]*da_dGroundState(cM[0],1,tt)*fE[0];
    } 
    else if (a == 2) {
      df = fM[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 3) {
      df = (c[4]*fE[1] + c[2])*da_dGroundState(cM[1],1,tt);
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 4) {
      df = fM[1]*fE[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 5) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
      df *= c[0]*fM[0]*da_dGroundState(cE[0],1,tsym);
    }
    else if (a == 6) {
      df = par*fE[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 7) {
      df = (c[4]*fM[1] + par*c[6])*da_dGroundState(cE[1],1,tsym);
      df *= c[0]*fM[0]*fE[0];
    }
#ifdef SEP_TWOPT_AMP
    else if (a == 8) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
      df *= c[0]*fE[0]*da_dGroundState(cM[0],0,tt)/(2.0*sqrt(c[8]));
    }
    else if (a == 9) {
      df = (c[4]*fE[1] + c[2])*da_dGroundState(cM[1],0,tt);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[9]));
    }
    else if (a == 10) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1];
      df *= c[0]*fM[0]*da_dGroundState(cE[0],0,tsym)/(2.0*sqrt(c[10]));
    }
    else if (a == 11) {
      df = (c[4]*fM[1] + par*c[6])*da_dGroundState(cE[1],0,tsym);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[11]));
    } 
#endif
    else {
      errExit(1,funcname,"parameter (=%d) exceeds the range [0,4]\n",a);
    }
  }
  else if ( state[0] == 3 ) {
    
    cM[0][1] = c[1];
    cM[1][1] = c[3];
    cM[2][1] = c[3]+c[9];
    
    cE[0][1] = c[5];
    cE[1][1] = c[7];
    cE[2][1] = c[7]+c[11];

#ifdef SEP_TWOPT_AMP
    // c[15] = |A0|^2 at p=0
    // c[16] = |A1|^2/|A0|^2 at p=0
    // c[17] = |A2|^2/|A0|^2 at p=0
    // c[18] = |A0|^2 at p!=0
    // c[19] = |A1|^2/|A0|^2 at p!=0
    // c[20] = |A2|^2/|A0|^2 at p!=0
    cM[0][0] = sqrt(c[15]); 
    cM[1][0] = sqrt(c[16]); 
    cM[2][0] = sqrt(c[17]); 
    
    cE[0][0] = sqrt(c[18]); 
    cE[1][0] = sqrt(c[19]); 
    cE[2][0] = sqrt(c[20]); 
#else
    cM[0][0] = 1.0; 
    cM[1][0] = 1.0; 
    cM[2][0] = 1.0; 
    
    cE[0][0] = 1.0; 
    cE[1][0] = 1.0; 
    cE[2][0] = 1.0; 
#endif
    
    for ( int i=0; i<state[0]; i++) {
      fM[i] = groundState(cM[i],tt);
      fE[i] = groundState(cE[i],tsym);
    }

    //f = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
    //        + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
    //        + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
    //f *= c[0]*fM[0]*fE[0];
    
    if (a == 0) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
              + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
              + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
      df *= fM[0]*fE[0];
    } 
    else if (a == 1) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
              + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
              + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
      df *= c[0]*da_dGroundState(cM[0],1,tt)*fE[0];
    } 
    else if (a == 2) {
      df = fM[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 3) {
      df = (c[4]*fE[1] + c[2] + par*c[14]*fE[2])*da_dGroundState(cM[1],1,tt);
      df += (c[13]*fE[2] + c[8] + c[12]*fE[1])*da_dGroundState(cM[2],1,tt);
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 4) {
      df = fM[1]*fE[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 5) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
              + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
              + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
      df *= c[0]*fM[0]*da_dGroundState(cE[0],1,tsym);
    }
    else if (a == 6) {
      df = par*fE[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 7) {
      df = (c[4]*fM[1] + par*c[6] + c[12]*fM[2])*da_dGroundState(cE[1],1,tsym);
      df += (c[13]*fM[2] + par*c[10] + par*c[14]*fM[1])*da_dGroundState(cE[2],1,tsym);
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 8) {
      df = fM[2];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 9) {
      df = (c[13]*fE[2] + c[8] + c[12]*fE[1])*da_dGroundState(cM[2],1,tt);
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 10) {
      df = par*fE[2];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 11) {
      df = (c[13]*fM[2] + par*c[10] + par*c[14]*fM[1])*da_dGroundState(cE[2],1,tsym);
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 12) {
      df = fM[2]*fE[1];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 13) {
      df = fM[2]*fE[2];
      df *= c[0]*fM[0]*fE[0];
    }
    else if (a == 14) {
      df = par*fM[1]*fE[2];
      df *= c[0]*fM[0]*fE[0];
    }
#ifdef SEP_TWOPT_AMP
    else if (a == 15) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
              + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
              + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
      df *= c[0]*da_dGroundState(cM[0],0,tt)*fE[0]/(2.0*sqrt(c[15]));
    }
    else if (a == 16) {
      df = (c[4]*fE[1] + c[2] + par*c[14]*fE[2])*da_dGroundState(cM[1],0,tt);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[16]));
    }
    else if (a == 17) {
      df = (c[13]*fE[2] + c[8] + c[12]*fE[1])*da_dGroundState(cM[2],0,tt);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[17]));
    }
    else if (a == 18) {
      df = 1.0 + c[4]*fM[1]*fE[1] + c[2]*fM[1] + par*c[6]*fE[1]
              + c[13]*fM[2]*fE[2] + c[8]*fM[2] + par*c[10]*fE[2]
              + par*c[14]*fM[1]*fE[2] + c[12]*fM[2]*fE[1];
      df *= c[0]*fM[0]*da_dGroundState(cE[0],0,tsym)/(2.0*sqrt(c[18]));
    }
    else if (a == 19) {
      df = (c[4]*fM[1] + par*c[6] + c[12]*fM[2])*da_dGroundState(cE[1],0,tsym);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[19]));
    }
    else if (a == 20) {
      df = (c[13]*fM[2] + par*c[10] + par*c[14]*fM[1])*da_dGroundState(cE[2],0,tsym);
      df *= c[0]*fM[0]*fE[0]/(2.0*sqrt(c[20]));
    }
#endif
    else {
      errExit(1,funcname,"parameter (=%d) exceeds the range [0,4]\n",a);
    }
  } else {
    errExit(101,funcname,
        "support up to 3 states but try %d states.\n",state[0]);
  }

  return df;
}
