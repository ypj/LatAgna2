double f_Lambda(const double *c, int i, const int nt, 
                const char *fit) 
{
  const char *funcname = "f_Lambda";
  
  double f = 0;

  if ( matchKey(fit,"constant") ) {
    f = c[0];
  }else{
    errExit(1,funcname,"Unknown fit function %s.\n",fit);
  }

  return f;
}


double da_f_Lambda(const double *c, int a, int i, const int nt, 
                const char *fit) 
{
  const char *funcname = "da_f_Lambda";
  
  double df = 0.0;
  
  if ( matchKey(fit,"constant") ) {
    df = 1.0;
  }else{
    errExit(1,funcname,"Unknown fit function %s.\n",fit);
  }

  return df;
}
