#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include "module-gsl/func_chisq.h"
#include "module-gsl/func_fit.h"

double GSL_TOL_MM_DELF;
double GSL_TOL_MM_GRAD;
double _GSL_TOL_MM_DELX;
double _GSL_TOL_MM_GRAD;
//double _GSL_TOL_MM_GRAD = 1.0e-02;

int _GSL_nlm_derivative(gsl_vector *c, int& iter_in, double& chi2, struct FitData const fit)
{

  _GSL_TOL_MM_DELX = GSL_TOL_MM_DELF;
  _GSL_TOL_MM_GRAD = GSL_TOL_MM_GRAD;
  
  int npar = fit.dim_min_dom;

  gsl_multimin_function_fdf chiSquared;
  chiSquared.n = npar;
  chiSquared.f = _GSL_chiSq;
  chiSquared.df = _GSL_dchiSq;
  chiSquared.fdf = _GSL_chiSq_dchiSq;
  chiSquared.params = (void*) &fit;

  const gsl_multimin_fdfminimizer_type *T;
#if 0
  T = gsl_multimin_fdfminimizer_conjugate_fr;
  T = gsl_multimin_fdfminimizer_steepest_descent;
#else
  T = gsl_multimin_fdfminimizer_vector_bfgs2;
#endif

  gsl_multimin_fdfminimizer *s;
  s = gsl_multimin_fdfminimizer_alloc(T,npar);

  //double step_size = 0.1*gsl_blas_dnrm2(c);
  //double lmin_tol = 0.1;
  // fit parameters are initialized to zero.
  double step_size = 5e-2;
  double lmin_tol = 2e-1;
#if 0
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.01, 1e-4);
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.01, 0.01);
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.5*gsl_blas_dnrm2(c), 1.0e-3);
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.01, 1.0e-3);
#else
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, step_size, lmin_tol);
#endif
  
  double f_old, f_new, grad;
  double delta_f = 0.0;
  double delta_x = 0.0;
  int MAXITER;
  int status;
  int restart = 1;
 
  f_old = s->f;
  fprintf(stdout,"f(initial) = %13.6le\n",f_old);
  fflush(stdout);
  MAXITER = iter_in;
  iter_in = 0;
  do {
    //if ( iter_in > 0 && (delta_f==0.0 && delta_x==0.0) ) {
    //  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, step_size*2, lmin_tol);
    //}

    status = gsl_multimin_fdfminimizer_iterate(s);
    iter_in++;
  
    f_new = s->f;
    delta_f = 2.0*fabs((f_old-f_new)/(f_old+f_new));
    delta_x = gsl_blas_dnrm2(s->dx)/gsl_blas_dnrm2(s->x);

    //grad = gsl_blas_dnrm2(s->gradient);
    size_t idx_grad_max = gsl_blas_idamax(s->gradient);
    grad = fabs(gsl_vector_get(s->gradient,idx_grad_max));
    
    if (status == GSL_ENOPROG) 
    {
      if (gsl_multimin_test_size(delta_x,_GSL_TOL_MM_DELX) == GSL_SUCCESS)
      {
        printf ("Minimum found at:\n");
        restart = 0;
      }
      else {
        printf("minimization can't progress\n");
      }
      break;
    }
    else if (status == GSL_SUCCESS)
    {
      if (delta_x==0.0 && delta_f==0.0) {
        if (gsl_multimin_test_size(grad,_GSL_TOL_MM_GRAD) == GSL_SUCCESS)
        {
          printf ("Minimum found at:\n");
          restart = 0;
        }
        else {
          printf("minimization can't progress\n");
          restart = 0;
        }
        break;
      }
      else {
        if (gsl_multimin_test_size(delta_f,GSL_TOL_MM_DELF) == GSL_SUCCESS &&
            gsl_multimin_test_size(grad,_GSL_TOL_MM_GRAD) == GSL_SUCCESS)
        {
          printf ("Minimum found at:\n");
          restart = 0;
          break;
        } 
        else {
          f_old = f_new;
        }
      }
    }
    else {
      printf("error: check iterator status (= %2d)\n",status);
      exit(1);
    }
  }
  while ( iter_in < MAXITER );

  chi2 = f_new;
  
  for ( int i=0; i<npar; i++ ) {
    gsl_vector_set(c,i,gsl_vector_get(s->x,i));
  }
 
  printf("iter = %d/%d\n"
         "f = %13.6le\n"
         "rel_del_f = %13.6le (tol = %g)\n"
         "rel_del_c = %13.6le (tol = %g)\n"
         "grad = %13.6le\n",
         iter_in, MAXITER,
         chi2, 
         delta_f, ((double)GSL_TOL_MM_DELF),
         delta_x, ((double)_GSL_TOL_MM_DELX),
         grad);

  printf("%11s %11s %11s %11s %13s %11s\n",
      "c[]", "g[]", "p[]", "w[]", "rel_del_c[]", "grad[]");
  for ( int i=0; i<npar; i++ ) {
    printf("%11.4g ",gsl_vector_get(s->x,i));
    printf("%11.4le ",gsl_vector_get(fit.g,fit.map_dom[i]));
    printf("%11.4le ",gsl_vector_get(fit.p,fit.map_dom[i]));
    printf("%11.4le ",1.0/gsl_vector_get(fit.iw,fit.map_dom[i]));
    printf("%13.4le ",gsl_vector_get(s->dx,i)/gsl_vector_get(s->x,i));
    printf("%11.4le",gsl_vector_get(s->gradient,i));
    printf("\n");
  }
  
  gsl_multimin_fdfminimizer_free(s);

  return restart;
}
