// dispfit.C
#include "latagna_env.h"
#include "global.h"
#include "io.h"
#include "setup.h"
#include "resample.h"
#include "fit.h"
#include "fitlib.h"
#include "matlib.h"
#include "error.h"
#include "correlator.h"
#include "strutil.h"
#include <cmath>

#include "tclap/CmdLine.h"
#include <iostream>
#include <string>

using namespace TCLAP;
using namespace std;

char infilename[100], filebasename[100];

int nfit, nmom, npa;
char ytype[50];

// data : Monte-Carlo samples for a correlator
// corr : ensemble averaged correlator
// cov  : covariance matrix, for the corr, between two physical time slices

int main(int argc, char* argv[]){
 
  const char *funcname = "dispfit";

  double dtmp;

  // parse command line arguments
  // and setup parameters
  //-----------------------------
  parseOption(argc,argv);
  
  // read correlator fit results
  //-----------------------------
  fhandle infile;

  mdouble data(nfit,nmom);

  infile.open(infilename,"r");
  readDataDispersion(data,infile,ytype);
  // plan: change to
  // infile.read(data);
  infile.close();

  // fitting
  //--------
  //int npa = NumParameters(fit); // number of fitting parameters
  
  mdouble disp(nmom);
  mdouble cov(nmom,nmom);
  mdouble momsq(nmom);
  mdouble momquad(nmom);

  double pxsq, pysq, pzsq;

  for(int k = 0; k < nmom; k++){
    pxsq = getFitDomain(GLB_DIR_X,k);
    pysq = getFitDomain(GLB_DIR_Y,k);
    pzsq = getFitDomain(GLB_DIR_Z,k);

    momsq.set(pxsq+pysq+pzsq,k);
    momquad.set(pxsq*pxsq+pysq*pysq+pzsq*pzsq,k);
  }

  mdouble c(npa);

  double cdf = 0.0; 
  double q = 0.0;

  int nsvd = -1;
  
  int iter = 0;

  dbgMsg(funcname,"fit = %s\n", fit);
  dbgMsg(funcname,"MINIMIZER = %s\n", MINIMIZER);
  dbgMsg(funcname,"fullcov = %s\n", fullcov);
 
  mdouble disp_avg(nmom), disp_err(nmom);
  mdouble c_fit(ns+1,npa), c_avg(npa), c_err(npa);
  mdouble cdf_fit(ns+1), q_fit(ns+1);
  double cdf_avg = 0.0, cdf_err = 0.0, q_avg = 0.0, q_err = 0.0;
  mdouble f_fit(ns+1,nmom), f_avg(nmom), f_err(nmom);

  c_fit.zerod();
  c_avg.zerod();
  c_err.zerod();

  estimateJkCov(data,cov);
  
  int npt = n2 - n1 + 1;    // number of data points used in fit
  mdouble invcov(npt,npt);  // inverse of covariance sub-matrix 
  invertCovMat(invcov,n1,cov,corflag,nsvd,maxmode,true);
  
  for( int del = -1; del < ns; del++ ){
  // del = -1 : the correlator fit result using all configuration data
  //            + covariance estimate using all resampled fit results
  // del = 0, 1, ... , ns-1 : fit using only resampled fit results
#if 0 
    resampleJKfromFIT(del,data,disp,cov);

    if( del == -1 ){
      for(int k = 0; k < nmom; k++){
        disp_avg.set(disp.get(k),k);
        disp_err.set(sqrt(cov.get(k,k)),k);
      }
    }
#else
    for(int k = 0; k < nmom; k++){
      disp.set(data.get(del+1,k),k);
    }

    if( del == -1 ){
      for(int k = 0; k < nmom; k++){
        disp_avg.set(disp.get(k),k);
        disp_err.set(sqrt(cov.get(k,k)),k);
      }
      putMsg(funcname,"Take Full Configuration:  del =%4d\n",del);
    }
    else{
      putMsg(funcname,"Generate Jacknife Sample:  del =%4d\n",del);
    }
#endif

    // garfield [fixme] implement the function that 
    // check and adjust fit range
    // fit.adustRange(n1,n2);

    // least chi-square fit
#ifdef USE_PYTHON_LSQFIT
    //
#else
    // Use the fit result without resampling as a initial guess
    // garfield [note] For the genlin, initial guess is not necessary.
    for (int i = 0; i < npa; i++){
      c.set(c_fit.get(0,i),i);
    }

    covfit(n1, fit, disp, invcov, c, c_guess, inv_width,
           cdf, q, MINIMIZER, corflag,
           nsvd, maxmode, nbayes, iter);
#endif
    
    // Store fit results
    for (int i = 0; i < npa; i++){
      c_fit.set(c.get(i),del+1,i);
    }
    cdf_fit.set(cdf,del+1);
    q_fit.set(q,del+1);

    // Evaluate fit function (and shift)
    f_fit.subarray(0,del+1);
    f_value(c,f_fit,0,nmom,fit);
    if ( matchKey(ytype,"identity-shift") || 
         matchKey(ytype,"square-shift") ) {
      dtmp = disp_avg.get(0);
      for(int k = 0; k < nmom; k++){
        f_fit.set(f_fit.get(k)+dtmp,k);
      }
    }
    f_fit.restore();
    
  } //for(del)
 
  // Shift the data point
  if ( matchKey(ytype,"identity-shift") || 
       matchKey(ytype,"square-shift") ) {
    dtmp = disp_avg.get(0);
    for(int k = 1; k < nmom; k++){
      disp_avg.set(disp_avg.get(k)+dtmp,k);
    }
  }
    
  // Convert fit parameters to masses
  convertToMass(c_fit,fit);

  // Compute JackKnife average and error estimate
  if( strcmp(resample,"jk") == 0 ){

    estimateJkVec(c_fit,c_avg,c_err);

    //estimateJk(cdf_fit,cdf_avg,cdf_err);
    estimateMeanStd(cdf_fit,cdf_avg,cdf_err);

    //estimateJk(q_fit,q_avg,q_err);
    estimateMeanStd(q_fit,q_avg,q_err);

    estimateJkVec(f_fit,f_avg,f_err);   
    
  }
  else if( strcmp(resample,"fit") == 0 ){

    for (int k = 0; k < nmom; k++) {
      putMsg(funcname,"%s%02d%s%12.5le%s%11.5le%s\n",
        "Disp[",k,"]  ",disp.get(k)," (",sqrt(cov.get(k,k)),")");
    }
  }

  // dump final results on screen 
  putMsg(funcname,"%12s%2d%4s%2d\n",
    "FR   =  ",n1," -- ",n2);
  
  putMsg(funcname,"%11s%6.3lf%s%5.3lf%s%15s%6.3lf\n",
    "CDF  = ",cdf_fit.get(0),"  (",cdf_err,")",
    "cf. avg = ",cdf_avg);
  
  putMsg(funcname,"%11s%7.4lf%s%6.4lf%s%14s%7.4lf\n",
    "Q    = ",q_fit.get(0)," (",q_err,")",
    "cf. avg = ",q_avg);
  
  for(int i = 0; i < npa; i++){
    putMsg(funcname,"%6s%d%s%12.5le%s%8.2le%s%14s%12.5le\n",
      "c[",i,"] = ",c_fit.get(0,i)," (",c_err.get(i),")",
      "cf. avg = ",c_avg.get(i));
  }

  // dump final results on file
  // The result file format is compatible with gnuplot and xmgrace.
  char outfilename[256];

  fhandle outfile;

  strcpy(outfilename,"disp_");
  strcat(outfilename,filebasename);
  
  putMsg(funcname,"%s%s\n",
    "Saving the fit results into the file ",outfilename);
  
  outfile.open(outfilename,"w");
  c_fit.subarray(0,0);
  writeHeader(filebasename,
              fit,n1,n2,fullcov,
              cdf_fit.get(0),cdf_err,
              q_fit.get(0),q_err,
              2, c_fit, c_err,
              outfile);
  c_fit.restore();
  f_fit.subarray(0,0);
  writeData(momsq,momquad,disp_avg,disp_err,f_fit,f_err,outfile);
  f_fit.restore();
  dumpJkSample(c_fit,0,1,outfile);
  outfile.close();
  
  // clean up
  clean();

  return 0;
}


// parse command line arguments
void parseOption(int argc, char** argv)
{
  const char *funcname = "parseOption";
  
  try {

    // Define arguments
    //-----------------
    CmdLine cmd("Commandline options",' ',"");

    ValueArg<string> arg_input_file("f","file",
            "input data file name",
            true,"","string",cmd);

    ValueArg<int> arg_data_samples("","samp",
            "number of samples",
            true,-1,"int",cmd);
    
    ValueArg<int> arg_data_dimension("","dim",
            "number of data points",
            true,-1,"int",cmd);
    
    ValueArg<string> arg_preprocess("","y-type",
            "data preprocessiong type",
            false,"identity","string",
            cmd);
    
    ValueArg<string> arg_fit_function("","func",
            "fit function type",true,"",
            "string",cmd);

    ValueArg<int> arg_range_lower_end("l","lower",
            "lower end of the fit range",
            true,-1,"int",cmd);

    ValueArg<int> arg_range_upper_end("u","upper",
            "upper end of the fit range",
            true,-1,"int",cmd);

    ValueArg<string> arg_minimizer("","min",
            "minimizer used to minimize chi square",
            false,"am+nt","string",
            cmd);

    ValueArg<double> arg_minimizer_tolerance("","mintol",
            "tolerance used to nonlinear minimizer",
            false,1.0e-10,"double",cmd);
    
    ValueArg<double> arg_minimizer_gradient_tolerance("","mingrad",
            "tolerance on the gradient used to nonlinear minimizer",
            false,1.0e-2,"double",cmd);

    ValueArg<double> arg_start_fudge_amoeba("","fudge",
            "start fudge factor for amoeba minimizer",
            false,1.5,"double",cmd);

    ValueArg<int> arg_max_iteration_amoeba("","iter_am",
            "maximum iterations for amoeba minimizer",
            false,9999,"int",cmd);
    
    ValueArg<int> arg_max_iteration_newton("","iter_nt",
            "maximum iterations for newton minimizer",
            false,2500,"int",cmd);

    ValueArg<double> arg_guess0("","c0",
            "Initial guess for coefficient c0",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess1("","c1",
            "Initial guess for coefficient c1",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess2("","c2",
            "Initial guess for coefficient c2",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess3("","c3",
            "Initial guess for coefficient c3",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess4("","c4",
            "Initial guess for coefficient c4",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess5("","c5",
            "Initial guess for coefficient c5",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess6("","c6",
            "Initial guess for coefficient c6",
            false,0.0,"double",cmd);

    ValueArg<double> arg_guess7("","c7",
            "Initial guess for coefficient c7",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior0("","p0",
            "Baysian proior for coefficient c0",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior1("","p1",
            "Baysian proior for coefficient c1",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior2("","p2",
            "Baysian proior for coefficient c2",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior3("","p3",
            "Baysian proior for coefficient c3",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior4("","p4",
            "Baysian proior for coefficient c4",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior5("","p5",
            "Baysian proior for coefficient c5",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior6("","p6",
            "Baysian proior for coefficient c6",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior7("","p7",
            "Baysian proior for coefficient c7",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width0("","iw0",
            "inverse of Baysian proior witdth for coefficient c0",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width1("","iw1",
            "inverse of Baysian proior witdth for coefficient c1",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width2("","iw2",
            "inverse of Baysian proior witdth for coefficient c2",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width3("","iw3",
            "inverse of Baysian proior witdth for coefficient c3",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width4("","iw4",
            "inverse of Baysian proior witdth for coefficient c4",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width5("","iw5",
            "inverse of Baysian proior witdth for coefficient c5",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width6("","iw6",
            "inverse of Baysian proior witdth for coefficient c6",
            false,0.0,"double",cmd);

    ValueArg<double> arg_prior_inv_width7("","iw7",
            "inverse of Baysian proior witdth for coefficient c7",
            false,0.0,"double",cmd);

    ValueArg<string> arg_cov("","cov",
            "covariance matrix dealing method",
            false,"diag","string",cmd);

    ValueArg<double> arg_svd_inverse_tolerance("","svdinv",
            "tolerance used to svd cutoff for matrix inversion",
            false,1.0e-10,"double",cmd);
    
    ValueArg<double> arg_svd_solution_tolerance("","svdsol",
            "tolerance used to svd cutoff for linear solver",
            false,1.0e-10,"double",cmd);

    ValueArg<int> arg_svd_max_mode("","svdmax",
            "maximum number of modes from SVD",false,-1,"int",cmd);

    ValueArg<string> arg_error_estimate("e","err",
            "error estimation method",false,"jk","string",cmd);

    // Parse the command line.
    cmd.parse(argc,argv);

    // Set variables
    //--------------
    strcpy(infilename,(arg_input_file.getValue()).c_str());
    //strcpy(filebasename,basename(infilename));
    
    nfit = arg_data_samples.getValue();
    nmom = arg_data_dimension.getValue();
    
    strcpy(ytype,(arg_preprocess.getValue()).c_str());

    strcpy(fit,(arg_fit_function.getValue()).c_str());
    
    npa = NumParameters(fit);
   
    int max_npa = 8;
    c_guess = (int *) malloc(sizeof(int)*max_npa);
    if (c_guess==NULL) errExit(7,funcname,"malloc failed\n");

    c_prior = (double *) malloc(sizeof(double)*max_npa);
    if (c_prior==NULL) errExit(7,funcname,"malloc failed\n");

    inv_width = (double *) malloc(sizeof(double)*max_npa);
    if (inv_width==NULL) errExit(7,funcname,"malloc failed\n");
    
    for (int i=0; i < max_npa; i++) { // initialize
      c_guess[i] = 0;
      c_prior[i] = 0.;
      inv_width[i] = 0.;
    }

    n1 = arg_range_lower_end.getValue();
    n2 = arg_range_upper_end.getValue();
    
    strcpy(MINIMIZER,(arg_minimizer.getValue()).c_str());

    TOL_FUNC_NL_MINIMIZER = arg_minimizer_tolerance.getValue();
    TOL_GRAD_NL_MINIMIZER = arg_minimizer_gradient_tolerance.getValue();

    StartFudge = arg_start_fudge_amoeba.getValue();
    
    MAX_ITER_AMOEBA = arg_max_iteration_amoeba.getValue();
    
    MAX_ITER_NEWTON = arg_max_iteration_newton.getValue();

    c_guess[0] = arg_guess1.getValue();
    c_guess[1] = arg_guess1.getValue();
    c_guess[2] = arg_guess2.getValue();
    c_guess[3] = arg_guess3.getValue();
    c_guess[4] = arg_guess4.getValue();
    c_guess[5] = arg_guess5.getValue();
    c_guess[6] = arg_guess6.getValue();
    c_guess[7] = arg_guess7.getValue();

    c_prior[0] = arg_prior0.getValue();
    c_prior[1] = arg_prior1.getValue();
    c_prior[2] = arg_prior2.getValue();
    c_prior[3] = arg_prior3.getValue();
    c_prior[4] = arg_prior4.getValue();
    c_prior[5] = arg_prior5.getValue();
    c_prior[6] = arg_prior6.getValue();
    c_prior[7] = arg_prior7.getValue();
    
    inv_width[0] = arg_prior_inv_width0.getValue();
    inv_width[1] = arg_prior_inv_width1.getValue();
    inv_width[2] = arg_prior_inv_width2.getValue();
    inv_width[3] = arg_prior_inv_width3.getValue();
    inv_width[4] = arg_prior_inv_width4.getValue();
    inv_width[5] = arg_prior_inv_width5.getValue();
    inv_width[6] = arg_prior_inv_width6.getValue();
    inv_width[7] = arg_prior_inv_width7.getValue();

    strcpy(fullcov,(arg_cov.getValue()).c_str());
    
    TOL_SVD_INV = arg_svd_inverse_tolerance.getValue();
    TOL_SVD_SOL = arg_svd_solution_tolerance.getValue();
    
    maxmode = arg_svd_max_mode.getValue();

    strcpy(resample,(arg_error_estimate.getValue()).c_str());

  } catch ( ArgException& e )
  { cout << "ERROR: " << e.error() << " " << e.argId() << endl; }
  
  setup();
}


// setup from arguments
void setup()
{
  const char *funcname = "setup";

  strcpy(filebasename,basename(infilename));

  TOL_AMOEBA = TOL_FUNC_NL_MINIMIZER;
  TOL_NEWTON = TOL_FUNC_NL_MINIMIZER;
  GSL_TOL_MM_DELF = TOL_FUNC_NL_MINIMIZER;
  GSL_TOL_MM_GRAD = TOL_GRAD_NL_MINIMIZER;
  if( strcmp(MINIMIZER,"am+nt") == 0 ){
    TOL_AMOEBA = sqrt(TOL_FUNC_NL_MINIMIZER);
  }
  //else{
  //  errExit(3,funcname,"invalid option for min\n");   
  //}

  if( strcmp(fullcov,"full") == 0 ){
    corflag = true;
  }
  else if( strcmp(fullcov,"diag") == 0 ){
    corflag = false;
  }
  else{
    errExit(1,funcname,"invalid option for cov\n"); 
  }
  
#if 1
//  if( corflag ){
    if( maxmode < 0 || maxmode > n2-n1+1 ){
      maxmode = n2-n1+1;
      warnMsg(funcname,
          "Check the input for number of mode to be retained after SVD.\n"
          "-> forced to %d, "
          "(the number of data points in the fit range)\n",maxmode);
    }
//  }
#else
  if( maxmode < 3 ){
    warnMsg(funcname,
        "resetting maxmode to the minimum allowed value 3.\n");
    maxmode = 3;
  }
#endif

  if( strcmp(resample,"jk") == 0 ){
    ns = nfit-1;
  }
  else if( strcmp(resample,"fit") == 0 ){
    ns = 0;
  }
  else{
    errExit(2,funcname,"invalid option for resample\n");    
  }
  
  // print the setup result
  putMsg(funcname,"nfit = %d\n", nfit);
  putMsg(funcname,"nmom = %d\n", nmom);
  putMsg(funcname,"n1 = %d\n", n1);
  putMsg(funcname,"n2 = %d\n", n2);
  putMsg(funcname,"fit = %s\n", fit);

  //int npa = NumParameters(fit);
  
  putMsg(funcname,"npa = %d\n", npa);
 
  nbayes = 0;
  for (int i = 0; i < npa; i++) {
    if (inv_width[i] != 0.0) {
      putMsg(funcname,"set prior for c[%d] = %10.3le, width = %10.3le\n",
          i,c_prior[i],1./inv_width[i]);
      nbayes++;
      //c_guess[i] = c_prior[i]; // set initial guess as the prior value 
    }
  }
  
  putMsg(funcname,"MINIMIZER = %s\n", MINIMIZER);
  putMsg(funcname,"StartFudge = %10.3le\n", StartFudge);
  putMsg(funcname,"TOL_AMOEBA = %10.3le\n", TOL_AMOEBA);
  putMsg(funcname,"TOL_NEWTON = %10.3le\n", TOL_NEWTON);
  putMsg(funcname,"TOL_GRAD   = %10.3le\n", TOL_GRAD_NL_MINIMIZER);
  putMsg(funcname,"MAX_ITER_AMOEBA = %d\n", MAX_ITER_AMOEBA);
  putMsg(funcname,"MAX_ITER_NEWTON = %d\n", MAX_ITER_NEWTON);
  putMsg(funcname,"fullcov = %s\n", fullcov);
  putMsg(funcname,"corflag = %d\n", corflag);
  putMsg(funcname,"TOL_SVD_INV = %10.3le\n", TOL_SVD_INV);
  putMsg(funcname,"TOL_SVD_SOL = %10.3le\n", TOL_SVD_SOL);
  putMsg(funcname,"maxmode = %d\n", maxmode);
  putMsg(funcname,"dof = %d\n", maxmode - npa + nbayes);
  putMsg(funcname,"resample = %s\n", resample);
  putMsg(funcname,"ns = %d\n", ns);
}

void clean() 
{
  const char *funcname = "clean";
  
  free(c_guess);
  c_guess = NULL;
  
  free(c_prior);
  c_prior = NULL;
  
  free(inv_width);
  inv_width = NULL;
}
