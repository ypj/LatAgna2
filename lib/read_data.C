// read_data.C
#include <cmath>
#include <string>
#include "io.h"
#include "strutil.h"
#include "error.h"
#include "setup.h"
#include "strutil.h"


int readData(multi1d<double>& data, fhandle& corfile, bool swap_re_im)
{
  int nt = data.len(1);
  return readData(data,corfile,nt,0,swap_re_im);
}


int readData(multi1d<double>& data, fhandle& corfile, int nt, int t_offset,
             bool swap_re_im)
{
  const char *funcname = "readData";

  int ncfg = data.len(0);
  //int nt = data.len(1);
 
  int t;
  double re, im;
  int re_index, im_index;

  if ( swap_re_im ) { // store z in order of Im(z), Re(z)
    re_index = 1;
    im_index = 0;
  } else { // store z in order of Re(z), Im(z)
    re_index = 0;
    im_index = 1;
  }

  putMsg(funcname,"Reading file %s, ncfg = %d, nt = %d\n",
                   corfile.name(),ncfg,nt);

  // skip the header line
  corfile.skipLine(1);

  for(int icfg = 0; icfg < ncfg; icfg++){
    // skip the configuration description
    corfile.skipLine(1);

    for(int it = 0; it < nt; it++){
      corfile.parseLine("%d %lf %lf", &t, &re, &im);
      
      // check time slice
      if( t != it ){
        errExit(1,funcname,"time =%d from file %s dosen't match to"
            "the required value %d\n",t,corfile.name(),it);
      }
 
      data.set(re,icfg,it+t_offset,re_index);
//#ifdef USE_IMAGINARY_PART
      data.set(im,icfg,it+t_offset,im_index);
//#endif

    }// for(it)
  }// for(icfg)

#if 0 // [debug]
  for(int icfg = 0; icfg < ncfg; icfg++){
    dbgMsg(funcname, "icfg =%4d\n",icfg);
    dbgMsg(funcname, "   t  re            im\n");
    for(int it = 0; it < nt; it++){
//#ifdef USE_IMAGINARY_PART
      dbgMsg(funcname, "%4d %13.6le %13.6le\n",
             it,data.get(icfg,it+t_offset,0),data.get(icfg,it+t_offset,1));
//#else
//      dbgMsg(funcname, "%4d %13.6le\n",
//           it,data.get(icfg,it+t_offset,0));
//#endif
    }
  }
#endif
  return 0;
}


int readDataDispersion(multi1d<double>& data, fhandle& infile, 
                       const char *ytype)
{
  const char *funcname = "readDataDispersion";

  int nfit = data.len(0);
  int nmom = data.len(1);
 
  int ifit;
  int px, py, pz;
  double E;
  double unit_pa;
  int NL;

  double dtmp;

  char tmpstr1[50], tmpstr2[50], tmpstr3[50];

  putMsg(funcname,"Reading file %s, nmom = %d, nfit = %d\n",
                   infile.name(),nmom,nfit);


  // read spatial lattice extent
  infile.parseLine("%s %d",tmpstr1,&NL);
  unit_pa = 2.*M_PI/(double)NL;

  for(int k = 0; k < nmom; k++){
    // skip the header line
    //infile.skipLine(11);

    // read momentum and set fit domain
    infile.parseLine("%s %d %s %d %s %d",
        tmpstr1,&px,tmpstr2,&py,tmpstr3,&pz);
    
    setFitDomain(pow(unit_pa*(double)px,2),GLB_DIR_X,k);
    setFitDomain(pow(unit_pa*(double)py,2),GLB_DIR_Y,k);
    setFitDomain(pow(unit_pa*(double)pz,2),GLB_DIR_Z,k);

    // read data
    for(int i = 0; i < nfit; i++){
      infile.parseLine("%d %lf",&ifit,&E);
      
      // check fit index
      if( i != ifit ){
        errExit(1,funcname,"%dth fit result is required. "
            "ifit =%d from file %s dosen't match to this.\n",
            i,ifit,infile.name());
      }
 
      if ( matchKey(ytype,"square") || 
           matchKey(ytype,"square-shift") ) {
        data.set(E*E,i,k);
      }
      else {
        data.set(E,i,k);
      }
      //data.set(E*E,i,k);

    }// for(i)
  }// for(k)
  
  if ( matchKey(ytype,"identity-shift") || 
       matchKey(ytype,"square-shift") ) {
    for(int i = 0; i < nfit; i++){
      dtmp = data.get(i,0);
      for(int k = 1; k < nmom; k++){
        data.set(data.get(i,k)-dtmp,i,k);
      }// for(k)
    }// for(i)
  }

#if 0 // [debug]
  for(int k = 0; k < nmom; k++){
    dbgMsg(funcname, "k =%4d\n",k);
    dbgMsg(funcname, "   i  E           "
                           "px^2        "
                           "py^2        "
                           "pz^2        \n");
    for(int i = 0; i < nfit; i++){
      dbgMsg(funcname, "%4d %13.6le %13.6le %13.6le %13.6le\n",
             i,data.get(i,k),
             getFitDomain(GLB_DIR_X,k),
             getFitDomain(GLB_DIR_Y,k),
             getFitDomain(GLB_DIR_Z,k));
    }
  }
#endif
  return 0;
}


int readRawData(mdouble& data, fhandle& rawfile, mstring& corrkey, 
                bool fresh)
{
  const char *funcname = "readRawData";

  int ncha = data.len(0);
  int nt   = data.len(1);

  int have_descrp = 1;
  int read_line = 1;

  char key[100] = "";
  char value[100] = "";

  int t;
  double re, im;
  
  //typedef struct{
  //  double re;
  //  double im;
  //}tslice;
      
  tslice *tslice_all = NULL;

  putMsg(funcname,"Reading file %s (fmt=%s)\n",
         rawfile.name(),rawfile.fmt());
  
  if ( matchKey(rawfile.fmt(),"default") ) { 
    have_descrp = 1;
    read_line = 1;
  } 
  else if ( matchKey(rawfile.fmt(),"lanl_ascii") ) {
    have_descrp = 0;
    read_line = 1;
    if ( fresh ) rawfile.skipLine(1);
  } 
  else if ( matchKey(rawfile.fmt(),"lanl_hdf5") ) {
    have_descrp = 0;
    read_line = 0;
  }else{
    errExit(2,funcname,"unsupported raw file format: %s\n",rawfile.fmt());
  }

  for(int icha = 0; icha < ncha; icha++){

    if ( have_descrp ) {
      // parse next header line in format "key value" 
      // until the value matches corrkey.
      while( strcmp(value,(corrkey.get(icha)).c_str()) != 0 ){
        rawfile.testLine("%s %s", &key, &value);
      }
      rawfile.skipLine(1);
    } 
   
    if ( read_line ) {
      for(int it = 0; it < nt; it++){
        rawfile.parseLine("%d %lf %lf", &t, &re, &im);
        
        // check time slice
        if( t != it ){
        errExit(1,funcname,"time =%d from file %s dosen't match to"
          "the required value %d\n",t,rawfile.name(),it);
        }
      
        data.set(re,icha,it,0);
        data.set(im,icha,it,1);
      }// for(it)
    } else {
      // read hdf5 data set in complex structure
      tslice_all = rawfile.h5dread_compound_tslice();

      // copy tslice_all to data
      for(int it = 0; it < nt; it++){
        data.set(tslice_all[it].re,icha,it,0);
        data.set(tslice_all[it].im,icha,it,1);
      }// for(it)
    }
  }// for(icha)

#if 0 // [debug]
  for(int icha = 0; icha < ncha; icha++){
    dbgMsg(funcname, "icha =%4d\n",icha);
    dbgMsg(funcname, "   t  re            im\n");
    for(int it = 0; it < nt; it++){
      dbgMsg(funcname, "%4d %13.6le %13.6le\n",
             it,data.get(icha,it,0),data.get(icha,it,1));
    }
  }
#endif

  return 0;
}


int readAux(multi1d<double>& aux, fhandle& auxfile, int ncol, int offset)
{
  const char *funcname = "readAux";

  int ns = aux.len(0);
  //int ncol = aux.len(1);
 
  int isamp;

  putMsg(funcname,"Reading file %s, ns = %d, ncol = %d\n",
                   auxfile.name(),ns,ncol);

  // skip the header line
  auxfile.skipLine(11);

  double *col = (double *) malloc(sizeof(double)*ncol);
  
  if (col == NULL)
    errExit(2,funcname,"malloc failed\n");

  for(int i = 0; i < ns; i++){

    isamp = auxfile.parseIndexedVarField(col,ncol);
      
    // check sample number
    if( isamp != i ){
      errExit(1,funcname,"isamp =%d from file %s dosen't match to"
          "the required value %d\n",isamp,auxfile.name(),i);
    }

    for(int j=0; j<ncol; j++) aux.set(col[j],isamp,j+offset);
#if 0 
    printf("%d",isamp);
    for(int j=0; j<ncol; j++) 
      printf(" %le",aux.get(isamp,j+offset));
    printf("\n");
#endif
  }

  free(col);
  col = NULL;

  return 0;
}


int readFitHeader(int set, fhandle& fitfile, 
                  char *channel, char *fit, 
                  int l[], int u[], int offset)
{
  const char *funcname = "readFitHeader";

  char key[25];
  int fr_l, fr_u;

  if (set==0) fitfile.skipLine(1);
  else fitfile.skipLine(2);

  fitfile.parseLine("%s %s", key, channel);
  //printf("key=%s,value=%s\n",key,channel);
  
  fitfile.parseLine("%s %s", key, fit);
  //printf("key=%s,value=%s\n", key, fit);
  
  fitfile.parseLine("%s %d %d", key, &fr_l, &fr_u);
  l[offset] = fr_l;
  u[offset] = fr_u;
  
  fitfile.skipLine(1);

  if (set==0) fitfile.skipLine(6);

  return 0;
}


int readFitData(multi1d<double>& c_fit, fhandle& fitfile, 
                int ncol, int offset)
{
  const char *funcname = "readFitData";

  int nrow = c_fit.len(0);
  //int ncol = c_fit.len(1);
 
  int isamp;

  putMsg(funcname,"Reading file %s, nrow = %d, ncol = %d\n",
                   fitfile.name(),nrow,ncol);

  double *col = (double *) malloc(sizeof(double)*ncol);
  
  if (col == NULL)
    errExit(2,funcname,"malloc failed\n");

  for(int i = 0; i < nrow; i++){

    isamp = fitfile.parseIndexedVarField(col,ncol);
      
    // check sample number
    if( isamp != i ){
      errExit(1,funcname,"isamp =%d from file %s dosen't match to"
          "the required value %d\n",isamp,fitfile.name(),i);
    }

    for(int j=0; j<ncol; j++) c_fit.set(col[j],isamp,j+offset);

  }

  free(col);
  col = NULL;

  return 0;
}
