// corfit.C
#include "latagna_env.h"
#include "global.h"
#include "io.h"
#include "resample.h"
#include "fit.h"
#include "fitlib.h"
#include "module-gsl/func_fit.h"
#include "matlib.h"
#include "error.h"
#include "strutil.h"
#include "correlator.h"
#include <cmath>
#include <time.h>

#include "tclap/CmdLine.h"
#include <iostream>
#include <string>

using namespace TCLAP;
using namespace std;

#define MAX_SMT 50 // number of data set for a simultaneous fit 
#define FILESTRLEN 100

int n_data;
char corfilename_smt[MAX_SMT][FILESTRLEN];
int nt_smt[MAX_SMT] = {0,};
char fit_smt[MAX_SMT][FITSTRLEN];
char alias_fit_smt[MAX_SMT][FITSTRLEN];
int l_smt[MAX_SMT] = {0,};
int u_smt[MAX_SMT] = {0,};
int npt_smt[MAX_SMT] = {0,};
int maxmode_smt[MAX_SMT] = {0,};

char filebasename[FILESTRLEN];
char *corfilename;
char corfiletype[10];
DATATYPE corr_data_type;
char symm[MAX_SMT][10];
bool symmflag[MAX_SMT] = {true,};
char swap_re_im[10];
bool swapflag[MAX_SMT] = {false,};

char auxfilename[MAX_SMT][FILESTRLEN];
int naux_file;
int naux_field[MAX_SMT+1] = {0,};
bool auxflag = false;

int ncfg, nt, ot, npa, opa;
int npt, opt;

int *c_fix_mask = NULL;
int nfix;
char fix_label[3];

void beginTimer(float *t_ref) 
{
  *t_ref = ( ((float) clock())/CLOCKS_PER_SEC);
}

void endTimer(const char *note, float t_ref) 
{
  float t_elapse;
  
  t_elapse = ( ((float) clock())/CLOCKS_PER_SEC - t_ref );

  printf ("%s : %f s\n",note,t_elapse);
}

//int itmp;
  
// data : Monte-Carlo samples for a correlator
// corr : ensemble averaged correlator
// cov  : covariance matrix, for the corr, between two physical time slices

int main(int argc, char* argv[])
{
  const char *funcname = "corfit";

  float time_total;
  beginTimer(&time_total);
  
  // parse command line arguments
  // and setup parameters
  //-----------------------------
  parseOption(argc,argv);

  // read correlator from file
  //--------------------------
  fhandle corfile;

//#ifdef USE_IMAGINARY_PART
  mdouble data(ncfg,nt,2);
//#else
//  mdouble data(ncfg,nt);
//#endif

  ot = 0;
  for(int i=0; i<n_data; i++) {
    corfile.open(corfilename_smt[i],"r");
    // append the next set of data
    readData(data,corfile,nt_smt[i],ot,swapflag[i]); 
    // plan: change to
    // correlator.read();
    corfile.close();

    // symmetrization
    // garfield [FIXME] make it an option by using the input argument --symm
    // plan: change to
    // correlator.symmetrize();
    //---------------
    if( symmflag[i] ) symmetrize(data,nt_smt[i],ot,symm[i]);
    
    ot += nt_smt[i]; // offset for the next set of data
  }
  
  // read auxiliary constraint data from file
  //------------------------------------------
  fhandle auxfile;

  mdouble c_aux(ns+1,naux_field[naux_file]);
 
  if ( auxflag ) {
    opa = 0;
    for (int i=0; i<naux_file; i++) {
      auxfile.open(auxfilename[i],"r");
      readAux(c_aux,auxfile,naux_field[i],opa);
      auxfile.close();
      opa += naux_field[i];
    }
  }
  


  // fitting
  //--------
  int itmp;
  
  mdouble corr(nt);
  mdouble cov(nt,nt);

  mdouble c(npa);

  double cdf = 0.0; 
  double q = 0.0;

  int nsvd = -1;

  //int iter;

  //dbgMsg(funcname,"fit = %s\n", fit);
  //dbgMsg(funcname,"MINIMIZER = %s\n", MINIMIZER);
  //dbgMsg(funcname,"fullcov = %s\n", fullcov);
 
  mdouble corr_avg(nt), corr_err(nt);
  mdouble c_fit(ns+1,npa), c_avg(npa), c_err(npa);
  mdouble cdf_fit(ns+1), q_fit(ns+1);
  double cdf_avg = 0.0, cdf_err = 0.0, q_avg = 0.0, q_err = 0.0;
  mdouble f_fit(ns+1,nt), f_avg(nt), f_err(nt);
  mdouble meff_fit(ns+1,nt), meff_avg(nt), meff_err(nt);
  mdouble meff_f_fit(ns+1,nt), meff_f_avg(nt), meff_f_err(nt);

  c_fit.zerod();
  c_avg.zerod();
  c_err.zerod();
  f_err.zerod();
  meff_fit.zerod();
  meff_f_fit.zerod();
  meff_avg.zerod();
  meff_err.zerod();
  meff_f_err.zerod();

  npt = 0;
  for(int i=0; i<n_data; i++) {
    // number of data points used in fit from i-th data set
    npt_smt[i] = u_smt[i] - l_smt[i] + 1; 
    npt += npt_smt[i]; 
  }
  
  mdouble covsub(npt,npt);  // covariance sub-matrix
  mdouble invcov(npt,npt);  // inverse of covariance sub-matrix

  for( int del = -1; del < ns; del++ ){
  // del = -1 : fit using the full samples
  // del = 0, 1, ... , ns-1 : fit using resampled correlator
    
    if ( corr_data_type == RAWJK ){
      resampleJKfromRAWJK(del,data,corr,cov);
    }
    else { // this is default mode.
      resampleJKfromRAW(del,data,corr,cov);
    }

    if( del == -1 ){
      for(int t = 0; t < nt; t++){
        corr_avg.set(corr.get(t),t);
        corr_err.set(sqrt(cov.get(t,t)),t);
      }
    }

    // garfield [fixme] implement the function that 
    // check and adjust fit range
    // fit.adustRange(n1,n2);

    // set parameter to initial guess value
    for (int i = 0; i < npa; i++){
      if (c_guess[i] && del == -1) {
        c.set(c_prior[i],i);
      } else {
        c.set(c_fit.get(0,i),i);
        // use the fit result without resampling as an initial guess
      }
    }
    // over write by fix condition
    for (int i = 0; i < nfix; i++) {
      c.set(c_aux.get(del+1,c_fix_mask[2*i+1]),c_fix_mask[2*i]);
    }
    // set equal parameters
    for (int i = 0; i < npa; i++){
      if (inv_width[i] <= -3.0) c.set(c.get(-(inv_width[i]+3)),i);
    }
    
    //npt = 0;
    //for(int i=0; i<n_data; i++) {
    //  // number of data points used in fit from i-th data set
    //  npt_smt[i] = u_smt[i] - l_smt[i] + 1; 
    //  npt += npt_smt[i]; 
    //}
  
    //mdouble invcov(npt,npt);  // inverse of covariance sub-matrix

#if 0
    invcov.zerod();
    ot = 0;
    opt = 0;

    for(int i=0; i<n_data; i++) {
      mdouble invcov_diag_block(npt_smt[i],npt_smt[i]);
      if(del == -1) {
        invertCovMat(invcov_diag_block,ot+l_smt[i],cov,corflag,nsvd,maxmode_smt[i],true);
      } else {
        invertCovMat(invcov_diag_block,ot+l_smt[i],cov,corflag,nsvd,maxmode_smt[i]);
      }
      
      for (int j=0; j<npt_smt[i]; j++) {
        for (int k=0; k<npt_smt[i]; k++) {
          invcov.set(invcov_diag_block.get(j,k),j+opt,k+opt);
        }
      }

      ot += nt_smt[i];
      opt += npt_smt[i];
    }
#else
    invcov.zerod();
    covsub.zerod();
    int i_ot, j_ot;
    int i_opt, j_opt;
    int IA, JB;
    
    i_ot = 0;
    i_opt = 0;
    for(int i_blk=0; i_blk<n_data; i_blk++) {
      j_ot = 0;
      j_opt = 0;
      for(int j_blk=0; j_blk<n_data; j_blk++) {
        
        if ( matchKey(alias_fit_smt[i_blk],alias_fit_smt[j_blk]) ) {
        // garfield [fixme] A more flexible control of covariance matrix
        // requires data block covariance matrix. Scale or turn off the
        // off-diagonal block for cross-correlation of different
        // correlators. Current implementation forces to ignore the
        // cross-correlation when two alias fit functions are different.
          IA = i_ot+l_smt[i_blk];
          for (int a=0; a<npt_smt[i_blk]; a++) {
            JB = j_ot+l_smt[j_blk];
            for (int b=0; b<npt_smt[j_blk]; b++) {
              covsub.set(cov.get(IA,JB),i_opt+a,j_opt+b);
              JB++;
            }
            IA++;
          }
        }

        j_ot += nt_smt[j_blk];
        j_opt += npt_smt[j_blk];
      }
      i_ot += nt_smt[i_blk];
      i_opt += npt_smt[i_blk];
    }
    
    if(del == -1) {
      invertCovMat(invcov,0,covsub,corflag,nsvd,maxmode,true);
    } else {
      invertCovMat(invcov,0,covsub,corflag,nsvd,maxmode);
    }
#endif
  
    // least chi-square fit
    //iter = 
    covfit_smt(n_data, l_smt, fit_smt, corr, nt_smt, invcov, npt_smt, 
               c, c_guess, c_prior, inv_width, 
               cdf, dof, q, MINIMIZER, corflag);

    // Store fit results
    for (int i = 0; i < npa; i++){
      c_fit.set(c.get(i),del+1,i);
    }
    cdf_fit.set(cdf,del+1);
    q_fit.set(q,del+1);

    f_fit.subarray(0,del+1);
    meff_f_fit.subarray(0,del+1);
    ot = 0;
    opa = 0;
    for (int i=0; i<n_data; i++) {
      mdouble f_fit_part(nt_smt[i]);
      itmp = NumParameters(fit_smt[i]);
      mdouble c_part(itmp);
      for( int k=0; k<itmp; k++ ) {
        c_part.set(c.get(opa+k),k);
      }
      f_value(c_part,f_fit_part,0,nt_smt[i],fit_smt[i]);
      for (int j=0; j<nt_smt[i]; j++) {
        f_fit.set(f_fit_part.get(j),ot+j);
      }
      // calculate effective mass from fit function
      //-------------------------------------------
      if ( calc_meff && GSL_isNpt(fit_smt[i]) == 2 ) {

        mdouble meff_f_fit_part(nt_smt[i]);

#ifdef AVGMEFF
        avgEffMass(f_fit_part,nt_smt[i],fit_smt[i],c.get(1),symmflag[i],meff_f_fit_part);
#else
        effMass(f_fit_part,nt_smt[i],fit_smt[i],symmflag[i],meff_f_fit_part);
#endif    

        for (int it=0; it<nt_smt[i]; it++) {
          meff_f_fit.set(meff_f_fit_part.get(it),ot+it);
        }
      
      }
      //-------------------------------------------
      ot += nt_smt[i];
      opa += itmp;
    }
    meff_f_fit.restore();
    f_fit.restore();

    meff_fit.subarray(0,del+1);
    ot = 0;
    for (int i=0; i<n_data; i++) {
      if ( calc_meff && GSL_isNpt(fit_smt[i]) == 2 ) {

        mdouble meff_fit_part(nt_smt[i]);
        mdouble corr_part(nt_smt[i]);
        
        for (int it=0; it<nt_smt[i]; it++) {
          corr_part.set(corr.get(ot+it),it);
        }
       
#ifdef AVGMEFF
        avgEffMass(corr_part,nt_smt[i],fit_smt[i],c.get(1),symmflag[i],meff_fit_part);
#else
        effMass(corr_part,nt_smt[i],fit_smt[i],symmflag[i],meff_fit_part);
#endif    
        
        for (int it=0; it<nt_smt[i]; it++) {
          meff_fit.set(meff_fit_part.get(it),ot+it);
        }
      
      }
      ot += nt_smt[i];
    }
    meff_fit.restore();

    //amp_fit.subarray(0,del+1);
    //effAmplitude(c,fit,nt,corr,amp_fit);
    //amp_fit.restore();

  } //for(del)
  
  // Convert fit parameter to amplitude
  opa = 0;
  for (int ip=0; ip<n_data; ip++) {
    itmp = NumParameters(fit_smt[ip]);

    if ( matchKey(fit_smt[ip],"newt+.v2") ) {
      for (int i = 0; i < ns+1; i++) {
        c_fit.set(exp(c_fit.get(i,opa+0)),i,opa+0);
        c_fit.set(-exp(c_fit.get(i,opa+2)),i,opa+2);
      }
    }
  
    opa += itmp;
  }

  // Compute JackKnife average and error estimate
  if( strcmp(resample,"jk") == 0 ){

    estimateJkVec(c_fit,c_avg,c_err);

    //estimateJk(cdf_fit,cdf_avg,cdf_err);
    estimateMeanStd(cdf_fit,cdf_avg,cdf_err);

    //estimateJk(q_fit,q_avg,q_err);
    estimateMeanStd(q_fit,q_avg,q_err);

    estimateJkVec(f_fit,f_avg,f_err);   
   
    if ( calc_meff ) {
      estimateJkVec(meff_fit,meff_avg,meff_err); 
      estimateJkVec(meff_f_fit,meff_f_avg,meff_f_err); 
    }

    //estimateJkVec(amp_fit,amp_avg,amp_err);
  }
  else if( strcmp(resample,"fit") == 0 ){

    int it, itsym, tsym;
    ot = 0;
    for (int i=0; i<n_data; i++) {
      putMsg(funcname,"\nData set %d\n",i);
      for (int t = 0; t <= nt_smt[i]/2; t++) {
        tsym = (nt_smt[i]-t)%nt_smt[i];
        it = t + ot;
        itsym = tsym + ot;
        putMsg(funcname,"%s%02d%s%12.5le%s%11.5le%s"
                        "%8s%02d%s%12.5le%s%11.5le%s\n",
          "Corr[",t,"]  ",corr.get(it)," (",sqrt(cov.get(it,it)),")",
          "Corr[",tsym,"]  ",corr.get(itsym),
          " (",sqrt(cov.get(itsym,itsym)),")");
      }
      ot += nt_smt[i];
    }
    printf("\n");
  }

#if 0
    for (int t = 0; t < nt; t++) {
      putMsg(funcname,"%s%02d%s%7.4lf%s%6.4lf%s%15s%7.4lf\n",
        "Z1[",t,"] = ",amp_fit.get(0,t)," (",amp_err.get(t),")",
        "cf. avg = ",amp_avg.get(t));
    }
#endif

  // dump final results on screen 
  for (int i=0; i<n_data; i++) {
    putMsg(funcname,"%12s%2d%4s%2d%6s%d%2s\n",
      "FR   =  ",l_smt[i]," -- ",u_smt[i]," (set ",i,") ");
  }
  
  putMsg(funcname,"%11s%6.3lf%s%5.3lf%s%24s%6.3lf%3s%3d\n",
    "CDF  = ",cdf_fit.get(0),"  (",cdf_err,")",
    "cf. chisq / dof = ",cdf_fit.get(0)*dof," / ",dof);
  
  putMsg(funcname,"%11s%7.4lf%s%6.4lf%s\n",
    "Q    = ",q_fit.get(0)," (",q_err,")");
  
  for(int i = 0; i < npa; i++){
    if (inv_width[i] == -1 || inv_width[i] == -2) {
      strcpy(fix_label,"F");
    } else {
      strcpy(fix_label," ");
    }
    putMsg(funcname,"%6s%d%s%12.5le%2s%8.2le%1s%3s\n",
      "c[",i,"] = ",c_fit.get(0,i)," (",c_err.get(i),")",fix_label);
  }

  // dump final results on file
  // The result file format is compatible with gnuplot and xmgrace.
  char outfilename[256];

  fhandle outfile;

  if (calc_meff) {
#ifdef AVGMEFF
    strcpy(outfilename,"avg_meff_");
#else
    strcpy(outfilename,"meff_");
#endif

    strcat(outfilename,filebasename);
    
    putMsg(funcname,"%s%s\n",
      "Saving the fit results into the file ",outfilename);
    
    outfile.open(outfilename,"w");
  
    meff_fit.subarray(0,0);
    meff_f_fit.subarray(0,0);
    ot = 0;
    for (int i=0; i<n_data; i++) {
      if ( GSL_isNpt(fit_smt[i]) == 2 ) {
        mdouble meff_fit_part(nt_smt[i]);
        mdouble meff_err_part(nt_smt[i]);
        mdouble meff_f_fit_part(nt_smt[i]);
        mdouble meff_f_err_part(nt_smt[i]);
  
        for (int it=0; it<nt_smt[i]; it++) {
          meff_fit_part.set(meff_fit.get(ot+it),it);
          meff_err_part.set(meff_err.get(ot+it),it);
          meff_f_fit_part.set(meff_f_fit.get(ot+it),it);
          meff_f_err_part.set(meff_f_err.get(ot+it),it);
        }
  
        c_fit.subarray(0,0);
        writeHeader(basename(corfilename_smt[i]),
                    fit_smt[i],l_smt[i],u_smt[i],fullcov,
                    cdf_fit.get(0),cdf_err,
                    q_fit.get(0),q_err,
                    2, c_fit, c_err,
                    outfile,(i==0));
        c_fit.restore();
  
        writeData(meff_fit_part,meff_err_part,
                  meff_f_fit_part,meff_f_err_part,outfile);
  
      }
      ot += nt_smt[i];
    }
    meff_f_fit.restore();
    meff_fit.restore();
    
    outfile.close();
  }

#ifndef AVGMEFF
  strcpy(outfilename,"fit_corr_");
  strcat(outfilename,filebasename);
  
  putMsg(funcname,"%s%s\n",
    "Saving the fit results into the file ",outfilename);
  
  outfile.open(outfilename,"w");
  opt = 0;
  for ( int i = 0; i<n_data; i++ ) {
  c_fit.subarray(0,0);
    writeHeader(basename(corfilename_smt[i]),
                fit_smt[i],l_smt[i],u_smt[i],fullcov,
                cdf_fit.get(0),cdf_err,
                q_fit.get(0),q_err,
                2, c_fit, c_err,
                outfile,(i==0));
    c_fit.restore();
    f_fit.subarray(0,0);
    writeData(corr_avg,corr_err,f_fit,f_err,outfile,opt,nt_smt[i]);
    f_fit.restore();
    opt += nt_smt[i];
  }
  outfile.close();

  strcpy(outfilename,"jk_fit_");
  strcat(outfilename,filebasename);
  
  putMsg(funcname,"%s%s\n",
    "Saving the fit results into the file ",outfilename);
  
  outfile.open(outfilename,"w");
  c_fit.subarray(0,0);
  for ( int i = 0; i<n_data; i++ ) {
    writeHeader(basename(corfilename_smt[i]),
                fit_smt[i],l_smt[i],u_smt[i],fullcov,
                cdf_fit.get(0),cdf_err,
                q_fit.get(0),q_err,
                2, c_fit, c_err,
                outfile,(i==0));
  }
  c_fit.restore();
  dumpJkSample(c_fit,outfile);
  outfile.close();
#endif

  // clean up
  clean();
  
  endTimer("Total Run Time",time_total);

  return 0;
}

//  fflush(stdout);
//  exit(1);


// parse command line arguments
void parseOption(int argc, char** argv)
{
  const char *funcname = "parseOption";

  try {

    // Define arguments
    //-----------------
    CmdLine cmd("Commandline options",' ',"");

    MultiArg<string> arg_input_file("f","dat",
            "input data file name",
            true,"string",cmd);
    
    ValueArg<string> arg_output_file("o","out",
            "output data file base name",
            false,"default","string",cmd);
#if 1 
    ValueArg<string> arg_aux_file("a","aux",
            "auxiliary resample fit data file name",
            false,"none","string",cmd);
    
    ValueArg<string> arg_aux_file2("b","aux2",
            "auxiliary resample fit data file name",
            false,"none","string",cmd);
#else
    MultiArg<string> arg_aux_file("a","aux",
            "auxiliary resample fit data file name",
            false,"string",cmd);
#endif
    ValueArg<int> arg_data_samples("","samp",
            "number of samples",
            true,-1,"int",cmd);
    
    MultiArg<int> arg_data_dimension("","dim",
            "number of data points",
            true,"int",cmd);
    
    ValueArg<int> arg_aux_dimension("","aux_nf",
            "number of data columns in auxiliary file",
            false,-1,"int",cmd);
    
    ValueArg<int> arg_aux_dimension2("","aux_nf2",
            "number of data columns in auxiliary file",
            false,-1,"int",cmd);
    
    MultiArg<string> arg_fit_function("","func",
            "fit function type",
            true,"string",cmd);

    MultiArg<int> arg_range_lower_end("l","lower",
            "lower end of the fit range",
            true,"int",cmd);

    MultiArg<int> arg_range_upper_end("u","upper",
            "upper end of the fit range",
            true,"int",cmd);
    
    MultiArg<string> arg_symmetrize_data("","symm",
            "symmetrize correlator",
            false,"string",cmd);
    
    MultiArg<string> arg_store_swapped_data("","swap",
            "store correlator in re/im swapped order",
            false,"string",cmd);
    
    ValueArg<string> arg_minimizer("","min",
            "minimizer used to minimize chi square",
            false,"am+nt","string",cmd);

    ValueArg<double> arg_minimizer_tolerance("","mintol",
            "tolerance used to nonlinear minimizer",
            false,1.0e-10,"double",cmd);
    
    ValueArg<double> arg_minimizer_gradient_tolerance("","mingrad",
            "tolerance on the gradient used to nonlinear minimizer",
            false,1.0e-2,"double",cmd);

    ValueArg<double> arg_start_fudge_amoeba("","fudge",
            "start fudge factor for amoeba minimizer",
            false,1.5,"double",cmd);
    
    ValueArg<int> arg_max_iteration_amoeba("","iter_am",
            "maximum iterations for amoeba minimizer",
            false,9999,"int",cmd);
    
    ValueArg<int> arg_max_iteration_newton("","iter_nt",
            "maximum iterations for newton minimizer",
            false,2500,"int",cmd);
    
    ValueArg<int> arg_max_iteration_gsl_deriv("","iter_fdf",
            "maximum iterations for gsl_deriv minimizer",
            false,10000,"int",cmd);
    
    MultiArg<string> arg_guess("c","guess",
            "Initial guess for coefficient c_i",
            false,"string",cmd);
    
    //MultiArg<string> arg_prior("p","prior",
    //        "Baysian proior for coefficient c_i",
    //        false,"string",cmd);
    
    MultiArg<string> arg_prior_width("w","width",
            "Baysian proior witdth for coefficient c_i",
            false,"string",cmd);
    
    MultiArg<string> arg_fix_fit_param("x","fix",
            "fix fit parameter c_i by j-th column data in aux file",
            false,"string",cmd);

    ValueArg<string> arg_cov("","cov",
            "covariance matrix dealing method",false,"diag","string",cmd);
    
    ValueArg<double> arg_svd_inverse_tolerance("","svdinv",
            "tolerance used to svd cutoff for matrix inversion",
            false,1.0e-10,"double",cmd);
    
    ValueArg<double> arg_svd_solution_tolerance("","svdsol",
            "tolerance used to svd cutoff for linear solver",
            false,1.0e-10,"double",cmd);

    MultiArg<int> arg_svd_max_mode("","svdmax",
            "maximum number of modes from SVD",false,"int",cmd);
    
    SwitchArg arg_calc_effective_mass("","meff",
            "calculate an effective mass",
            cmd,false);

    ValueArg<string> arg_error_estimate("e","err",
            "error estimation method",
            false,"jk","string",cmd);
    
    ValueArg<string> arg_input_data_type("","dtype",
            "input data type",
            false,"raw","string",cmd);

    // Parse the command line.
    cmd.parse(argc,argv);
    
    // Set variables
    //--------------
    vector<string> vs = arg_input_file.getValue();
    n_data = static_cast<int> (vs.size());
    for (int i=0; i < n_data; i++) {
      strcpy(corfilename_smt[i],vs[i].c_str());
      printf("-f %d/%d %s\n",i+1,n_data,corfilename_smt[i]);
    }
    
    //-------------------------------------------------------------- 
    strcpy(filebasename,(arg_output_file.getValue()).c_str());
    printf("filebasname=%s\n",filebasename);
    
    //-------------------------------------------------------------- 
#if 1
    naux_file = 0;

    strcpy(auxfilename[0],(arg_aux_file.getValue()).c_str());
    if ( ! matchKey(auxfilename[0],"none") ) {
     naux_file++;
    }
    printf("--aux %s\n",auxfilename[0]);
    
    strcpy(auxfilename[1],(arg_aux_file2.getValue()).c_str());
    if ( ! matchKey(auxfilename[1],"none") ) {
     naux_file++;
    }
    printf("--aux2 %s\n",auxfilename[1]);
#else
    vs = arg_aux_file.getValue();
    naux_file = static_cast<int> (vs.size());
    for (int i=0; i < naux_file; i++) {
      strcpy(auxfilename[i],vs[i].c_str());
      printf("-a %d/%d %s\n",i+1,naux_file,auxfilename[i]);
    }
#endif
    
    //-------------------------------------------------------------- 
    ncfg = arg_data_samples.getValue();
    
    //-------------------------------------------------------------- 
    int itmp;

    vector<int> vi = arg_data_dimension.getValue();
    itmp = static_cast<int> (vi.size());
    if (itmp != 1 && itmp != n_data)
      errExit(2,funcname,
          "--dim: use same parameter or provide parameter for each data set\n");
    for (int i=0; i < n_data; i++) {
      (itmp == 1) ? nt_smt[i] = vi[0] : nt_smt[i] = vi[i];
      //printf("--dim %d/%d %d\n",i+1,itmp,nt_smt[i]);
    }
    
    //-------------------------------------------------------------- 
    for ( int i=0; i<naux_file; i++) {
      naux_field[i] = -1; // initialize
    }
    naux_field[0] = arg_aux_dimension.getValue();
    naux_field[1] = arg_aux_dimension2.getValue();
    
    //-------------------------------------------------------------- 
    vs = arg_fit_function.getValue();
    itmp = static_cast<int> (vs.size());
    if (itmp != 1 && itmp != n_data)
      errExit(3,funcname,
          "--func: use same parameter or provide parameter for each data set\n");
    for (int i=0; i < n_data; i++) {
      (itmp == 1) ? strcpy(alias_fit_smt[i],vs[0].c_str())
                  : strcpy(alias_fit_smt[i],vs[i].c_str());
      int alias_num;
      std::string alias_str(alias_fit_smt[i]);
      size_t substr_pos = alias_str.find("-alias");
      if (substr_pos == std::string::npos) {
        strcpy(fit_smt[i],alias_fit_smt[i]);
      } else {
        strcpy(fit_smt[i],alias_str.substr(0,substr_pos).c_str());
      }
      printf("--func (alias) %d/%d %s\n",i+1,itmp,alias_fit_smt[i]);
      printf("--func %d/%d %s\n",i+1,itmp,fit_smt[i]);
    }
    
    npa = NumParameters(fit_smt,n_data);

    c_guess = (int *) malloc(sizeof(int)*npa);
    if (c_guess==NULL) errExit(7,funcname,"malloc failed\n");

    c_prior = (double *) malloc(sizeof(double)*npa);
    if (c_prior==NULL) errExit(7,funcname,"malloc failed\n");

    inv_width = (double *) malloc(sizeof(double)*npa);
    if (inv_width==NULL) errExit(7,funcname,"malloc failed\n");
    
    for (int i=0; i < npa; i++) { // initialize
      c_guess[i] = 0;
      c_prior[i] = 0.;
      inv_width[i] = 0.;
    }
    
    //-------------------------------------------------------------- 
    vi = arg_range_lower_end.getValue();
    itmp = static_cast<int> (vi.size());
    if (itmp != 1 && itmp != n_data)
      errExit(4,funcname,
          "-l: use same parameter or provide parameter for each data set\n");
    for (int i=0; i < n_data; i++) {
      (itmp == 1) ? l_smt[i] = vi[0] : l_smt[i] = vi[i];
      //printf("-l %d/%d %d\n",i+1,itmp,l_smt[i]);
    }
    
    //-------------------------------------------------------------- 
    vi = arg_range_upper_end.getValue();
    itmp = static_cast<int> (vi.size());
    if (itmp != 1 && itmp != n_data)
      errExit(5,funcname,
          "-u: use same parameter or provide parameter for each data set\n");
    for (int i=0; i < n_data; i++) {
      (itmp == 1) ? u_smt[i] = vi[0] : u_smt[i] = vi[i];
      //printf("-u %d/%d %d\n",i+1,itmp,u_smt[i]);
    }
    
    //-------------------------------------------------------------- 
    //strcpy(symm,(arg_symmetrize_data.getValue()).c_str());
    vs = arg_symmetrize_data.getValue();
    itmp = static_cast<int> (vs.size());
    if (itmp == 0){
      for (int i=0; i < n_data; i++) symmflag[i] = true;
    } 
    else {
      if (itmp != 1 && itmp != n_data)
        errExit(3,funcname,
            "--symm: use same parameter or provide parameter for each data set\n");
      for (int i=0; i < n_data; i++) {
        (itmp == 1) ? strcpy(symm[i],vs[0].c_str())
                    : strcpy(symm[i],vs[i].c_str());
        
        if ( matchKey(symm[i],"yes") ) {
          symmflag[i] = true;
        }
        else if ( matchKey(symm[i],"apr") ) {
          symmflag[i] = true;
        }
        else if ( matchKey(symm[i],"tsep") ) {
          symmflag[i] = true;
        }
        else if ( matchKey(symm[i],"no") ) {
          symmflag[i] = false;
        } 
        else {
          errExit(1,funcname,"invalid option for symm\n");    
        }
      }
    }
    
    //-------------------------------------------------------------- 
    //strcpy(swap_re_im,(arg_store_swapped_data.getValue()).c_str());
    vs = arg_store_swapped_data.getValue();
    itmp = static_cast<int> (vs.size());
    if (itmp == 0){
      for (int i=0; i < n_data; i++) swapflag[i] = false;
    } 
    else {
      if (itmp != 1 && itmp != n_data)
        errExit(3,funcname,
            "--swap: use same parameter or provide parameter for each data set\n");
      for (int i=0; i < n_data; i++) {
        (itmp == 1) ? strcpy(swap_re_im,vs[0].c_str())
                    : strcpy(swap_re_im,vs[i].c_str());
        
        if ( matchKey(swap_re_im,"yes") ) {
          swapflag[i] = true;
        }
        else if ( matchKey(swap_re_im,"no") ) {
          swapflag[i] = false;
        } 
        else {
          errExit(1,funcname,"invalid option for swap\n");    
        }
      }
    }
   
    //-------------------------------------------------------------- 
    strcpy(MINIMIZER,(arg_minimizer.getValue()).c_str());

    //-------------------------------------------------------------- 
    TOL_FUNC_NL_MINIMIZER = arg_minimizer_tolerance.getValue();
    
    //-------------------------------------------------------------- 
    TOL_GRAD_NL_MINIMIZER = arg_minimizer_gradient_tolerance.getValue();

    //-------------------------------------------------------------- 
    StartFudge = arg_start_fudge_amoeba.getValue();
    
    //-------------------------------------------------------------- 
    MAX_ITER_AMOEBA = arg_max_iteration_amoeba.getValue();
    
    //-------------------------------------------------------------- 
    MAX_ITER_NEWTON = arg_max_iteration_newton.getValue();

    //-------------------------------------------------------------- 
    MAX_ITER_GSL_DERIV = arg_max_iteration_gsl_deriv.getValue();

    //-------------------------------------------------------------- 
    vs = arg_guess.getValue();
    itmp = static_cast<int> (vs.size());
    for (int i=0; i<itmp; i++) {
      int idx;
      double value;
      
      if ( sscanf(vs[i].c_str(),"%d:%lf",&idx,&value) !=2 )
        errExit(8,funcname,"invalid format %s. use -c index:value\n",
            vs[i].c_str());
      if ( idx < 0 || idx >= npa )
        errExit(9,funcname,"invalid fit parameter index %d\n",idx);
      
      c_guess[idx] = 1;
      c_prior[idx] = value;
      //printf("-c %d:%le\n",idx,c_prior[idx]);
    }
    
    //-------------------------------------------------------------- 
    //vs = arg_prior.getValue();
    //itmp = static_cast<int> (vs.size());
    //for (int i=0; i<itmp; i++) {
    //  int idx;
    //  double value;
    //  
    //  if ( sscanf(vs[i].c_str(),"%d:%lf",&idx,&value) !=2 )
    //    errExit(8,funcname,"invalid format %s. use --prior index:value\n",
    //        vs[i].c_str());
    //  if ( idx < 0 || idx >= npa )
    //    errExit(9,funcname,"invalid fit parameter index %d\n",idx);
    //  
    //  c_prior[idx] = value;
    //  printf("--prior %d:%le\n",idx,c_prior[idx]);
    //}
    
    //-------------------------------------------------------------- 
    vs = arg_prior_width.getValue();
    itmp = static_cast<int> (vs.size());
    for (int i=0; i<itmp; i++) {
      int idx;
      double value;
      
      if ( sscanf(vs[i].c_str(),"%d:%lf",&idx,&value) !=2 )
        errExit(8,funcname,"invalid format %s. use --width index:value\n",
            vs[i].c_str());
      if ( idx < 0 || idx >= npa )
        errExit(9,funcname,"invalid fit parameter index %d\n",idx);
      
      if ( value == 0.0 ) {
        inv_width[idx] = -1.0; // flag: infinity by the zero width
      }
      else if ( value <= -3.0 ) {
        inv_width[idx] = value; // flag: set equal parameters
      }
      else {
        if (value < 0.0) { 
          value = -value;
          warnMsg(funcname,"reset negative prior witdth (=%s) from input to its absolute value\n",vs[i].c_str());
        }
        inv_width[idx] = 1.0/value;
      }

      if ( value > -3.0 && !c_guess[idx] ) {
        warnMsg(funcname,"prior value for parameter c[%d] does not provided. --> forced to use default guess value.\n",idx);
      }
      //printf("--width %d:%le\n",idx,1.0/inv_width[idx]);
    }

    //-------------------------------------------------------------- 
    vs = arg_fix_fit_param.getValue();
    nfix = static_cast<int> (vs.size());
    if (nfix !=0) {
      c_fix_mask = (int *) malloc(sizeof(int)*2*nfix);
      if (c_fix_mask==NULL) errExit(7,funcname,"malloc failed\n");
    }
    for (int i=0; i<nfix; i++) {
      int c_idx, f_idx, a_idx;
      if ( sscanf(vs[i].c_str(),"%d:%d:%d",&c_idx,&a_idx,&f_idx) !=3 ) {
        if ( sscanf(vs[i].c_str(),"%d:%d",&c_idx,&f_idx) !=2 ) {
          errExit(8,funcname,"invalid fix format %s. use --fix index:field\n",vs[i].c_str());
        }
        a_idx = 0; // use a single aux_file
      }
      if ( c_idx < 0 || c_idx >= npa )
        errExit(9,funcname,"invalid fit parameter index %d\n",c_idx);
      if ( a_idx < 0 || a_idx >= naux_file )
        errExit(9,funcname,"invalid aux file index %d\n",a_idx);
      if ( f_idx < 0 || f_idx >= naux_field[a_idx] )
        errExit(10,funcname,"invalid aux file field index %d\n",f_idx);
      
      c_fix_mask[2*i] = c_idx;
      if (a_idx == 1) {
        c_fix_mask[2*i+1] = f_idx + naux_field[0];
      } else {
        c_fix_mask[2*i+1] = f_idx; // if a_idx == 0
      }

      if ( inv_width[c_idx] == -1.0 || inv_width[c_idx] > 0.0)
        warnMsg(funcname,"Baysian constraint -w %d:%le is ignored and forced to use fix condition -x %d:%d\n",c_idx,1/inv_width[c_idx],c_idx,f_idx);
      
      if ( inv_width[c_idx] <= -3.0 )
        errExit(20,funcname,"Conflit: try to impose the fixing condition on the c[%]\n",(int)(-(inv_width[c_idx]+3)));

      inv_width[c_idx] = -2.0; // flag: fixing by aux resample data

      if ( c_guess[c_idx] ) {
        warnMsg(funcname,"Initial guess -c %d:%le is ignored and forced to use fix condition -x %d:%d\n",c_idx,c_prior[c_idx],c_idx,f_idx);
        c_prior[c_idx] = 0.0;
      }
      c_guess[c_idx] = 1;
      printf("--fix %d:%d\n",c_fix_mask[2*i],c_fix_mask[2*i+1]);
    }

    //-------------------------------------------------------------- 
    strcpy(fullcov,(arg_cov.getValue()).c_str());
    
    //-------------------------------------------------------------- 
    TOL_SVD_INV = arg_svd_inverse_tolerance.getValue();
    
    //-------------------------------------------------------------- 
    TOL_SVD_SOL = arg_svd_solution_tolerance.getValue();
    
    //-------------------------------------------------------------- 
    //maxmode = arg_svd_max_mode.getValue();
    vi = arg_svd_max_mode.getValue();
    itmp = static_cast<int> (vi.size());
    if (itmp == 0) { // set default value
      for (int i=0; i < n_data; i++) {
        maxmode_smt[i] = -1;
        printf("--svdmax %d/%d %d\n",i+1,itmp,maxmode_smt[i]);
      }
    }
    else if (itmp == 1 || itmp == n_data) {
      for (int i=0; i < n_data; i++) {
        (itmp == 1) ? maxmode_smt[i] = vi[0] : maxmode_smt[i] = vi[i];
        printf("--svdmax %d/%d %d\n",i+1,itmp,maxmode_smt[i]);
      }
    } else {
      errExit(7,funcname,
          "--svdmax: use same parameter or provide parameter for each data set\n");
    }

    //-------------------------------------------------------------- 
    calc_meff = arg_calc_effective_mass.getValue();
    
    //-------------------------------------------------------------- 
    strcpy(resample,(arg_error_estimate.getValue()).c_str());
    
    //-------------------------------------------------------------- 
    strcpy(corfiletype,(arg_input_data_type.getValue()).c_str());

  } catch ( ArgException& e )
  { cout << "ERROR: " << e.error() << " " << e.argId() << endl; }

  setup();
}


// setup from arguments
void setup()
{
  const char *funcname = "setup";

  int itmp;
  
  if ( matchKey(filebasename,"default") )
    strcpy(filebasename,basename(corfilename_smt[0]));

#if 0
  if ( matchKey(auxfilename[0],"none") ) {
#else
  if ( naux_file == 0 ) {
#endif
    auxflag = false;
    naux_field[naux_file] = 1;
  } 
  else {
    auxflag = true;
    //naux_field[naux_file] = naux_field[0];
    itmp = 0;
    for ( int i=0; i<naux_file; i++) {
      itmp += naux_field[i];
      if (naux_field[i] <= 0) 
        errExit(2,funcname,"provide a number of columns in the aux data file; use option --aux_nf\n");
    }
    naux_field[naux_file] = itmp;
    if (c_fix_mask==NULL) 
      errExit(1,funcname,"provide a fix condition; use option --fix\n");
  }

  
  //if ( matchKey(symm,"yes") ) {
  //  symmflag = true;
  //}
  //else if ( matchKey(symm,"no") ) {
  //  symmflag = false;
  //} 
  //else if ( matchKey(symm,"tsep") ) {
  //  symmflag = true;
  //} 
  //else if ( matchKey(symm,"apr") ) {
  //  symmflag = true;
  //} 
  //else {
  //  errExit(1,funcname,"invalid option for symm\n");    
  //}
  
  //if ( matchKey(swap_re_im,"yes") ) {
  //  swapflag = true;
  //}
  //else if ( matchKey(swap_re_im,"no") ) {
  //  swapflag = false;
  //} 
  //else {
  //  errExit(1,funcname,"invalid option for swap\n");    
  //}

  TOL_AMOEBA = TOL_FUNC_NL_MINIMIZER;
  TOL_NEWTON = TOL_FUNC_NL_MINIMIZER;
  GSL_TOL_MM_DELF = TOL_FUNC_NL_MINIMIZER;
  GSL_TOL_MM_GRAD = TOL_GRAD_NL_MINIMIZER;
  if( matchKey(MINIMIZER,"am+nt") ){
    TOL_AMOEBA = sqrt(TOL_FUNC_NL_MINIMIZER);
  }

  if( matchKey(fullcov,"full") ){
    corflag = true;
  }
  else if( matchKey(fullcov,"diag") ){
    corflag = false;
  }
  else{
    errExit(1,funcname,"invalid option for cov\n"); 
  }
 
  for( int i=0; i<n_data; i++ ) {
    if( maxmode_smt[i] < 0 || maxmode_smt[i] > u_smt[i]-l_smt[i]+1 ){
      maxmode_smt[i] = u_smt[i]-l_smt[i]+1;
      warnMsg(funcname,
          "Check the input for number of mode to be retained after SVD.\n"
          "-> forced to %d, "
          "(the number of data points in the fit range)\n",maxmode_smt[i]);
    }
  }

  // mask common fit parameters
  int oi = 0;
  int oj;
  for ( int i=0; i<n_data; i++ ) {
    // inv_width <= -3
    oj = 0;
    for ( int j=0; j<=i; j++ ) {
      oj += NumParameters(fit_smt[j]);
    }
    for ( int j=i+1; j<n_data; j++ ) {
      if ( matchKey(alias_fit_smt[j],alias_fit_smt[i]) ) {
        for ( int a=0; a<NumParameters(fit_smt[i]); a++ ) {
          //c_guess[a+oj] = c_guess[a+oi]; 
          //c_prior[a+oj] = c_prior[a+oi]; 
          inv_width[a+oj] = -(a+oi+3); 
        }
        break;
      }
      oj += NumParameters(fit_smt[j]);
    }
    oi += NumParameters(fit_smt[i]);
  }

  if( strcmp(resample,"jk") == 0 ){
    ns = ncfg;
  }
  else if( strcmp(resample,"fit") == 0 ){
    ns = 0;
  }
  else{
    errExit(2,funcname,"invalid option for resample\n");    
  }
  
  if( strcmp(corfiletype,"raw") == 0 ){
    corr_data_type = RAW;
  }
  else if( strcmp(corfiletype,"rawjk") == 0 ){
    corr_data_type = RAWJK;
  }
  else{
    errExit(2,funcname,"invalid option for data type\n");    
  }
  
  // print the setup result
  putMsg(funcname,"ncfg = %d\n", ncfg);

  maxmode = 0;
  for (int i=0; i<n_data; i++) {
    putMsg(funcname,"-------------\ndata set = %d\n-------------\n",i+1);
    putMsg(funcname,"nt = %d\n", nt_smt[i]);
    putMsg(funcname,"n1 = %d\n", l_smt[i]);
    putMsg(funcname,"n2 = %d\n", u_smt[i]);
    putMsg(funcname,"fit = %s\n", alias_fit_smt[i]);
    putMsg(funcname,"maxmode = %d\n", maxmode_smt[i]);
    maxmode += maxmode_smt[i];
  }
  putMsg(funcname,"-------------\n");

  nt = 0;
  for (int i=0; i<n_data; i++) nt += nt_smt[i];
  putMsg(funcname,"nt (total) = %d\n", nt);
  //nt = nt_smt[0];
  //for (int i=1; i<n_data; i++) {
  //  if ( nt_smt[0] != nt )
  //    errExit(3,funcname,"variable nt is not supported\n");
  //    // garfield [fixme] 
  //    // nt_smt is MultiArg type, but variable nt is not fully implemented.
  //}
 
  int npa_true = npa - nfix;
  int nconst = 0;
  nbayes = 0;
  for (int i = 0; i < npa; i++) {
    if (inv_width[i] > 0.0) {
      putMsg(funcname,"set prior for c[%d] = %10.3le, width = %10.3le\n",
          i,c_prior[i],1./inv_width[i]);
      nbayes++;
    }
    if (inv_width[i] == -1) { 
      nconst++; // number of fixed parameters by zero prior width
      npa_true--;
    }
    if (inv_width[i] <= -3.0) {
      putMsg(funcname,"c[%d] is declared to be equal to c[%d].\n",
            i,(int)(-(inv_width[i]+3)));
      npa_true--;
    }
  }
  dof = maxmode - npa_true + nbayes;

  for (int i = 0; i < nfix; i++) {
    putMsg(funcname,"fix parameter c[%d] from aux file column %d\n",
           c_fix_mask[2*i],c_fix_mask[2*i+1]);
  }
  
  putMsg(funcname,"MINIMIZER = %s\n", MINIMIZER);
  putMsg(funcname,"StartFudge = %10.3le\n", StartFudge);
  putMsg(funcname,"TOL_AMOEBA = %10.3le\n", TOL_AMOEBA);
  putMsg(funcname,"TOL_NEWTON = %10.3le\n", TOL_NEWTON);
  putMsg(funcname,"TOL_GRAD   = %10.3le\n", TOL_GRAD_NL_MINIMIZER);
  putMsg(funcname,"MAX_ITER_AMOEBA = %d\n", MAX_ITER_AMOEBA);
  putMsg(funcname,"MAX_ITER_NEWTON = %d\n", MAX_ITER_NEWTON);
  putMsg(funcname,"MAX_ITER_GSL_DERIV= %d\n", MAX_ITER_GSL_DERIV);
  putMsg(funcname,"fullcov = %s\n", fullcov);
  putMsg(funcname,"corflag = %d\n", corflag);
  putMsg(funcname,"TOL_SVD_INV = %10.3le\n", TOL_SVD_INV);
  putMsg(funcname,"TOL_SVD_SOL = %10.3le\n", TOL_SVD_SOL);
  putMsg(funcname,"maxmode (total) = %d\n", maxmode);
  putMsg(funcname,"npa = %d\n", npa_true);
  putMsg(funcname,"nbayes = %d\n", nbayes);
  putMsg(funcname,"nbayes(const) = %d\n", nconst);
  putMsg(funcname,"nfix = %d\n", nfix);
  putMsg(funcname,"dof = %d\n", dof);
  putMsg(funcname,"resample = %s\n", resample);
  putMsg(funcname,"datatype = %s\n", corfiletype);
  putMsg(funcname,"ns = %d\n", ns);
 
}
  

void clean() 
{
  const char *funcname = "clean";
  
  if ( nfix != 0 ) {
    free(c_fix_mask);
    c_fix_mask=NULL;
  }

  free(c_guess);
  c_guess = NULL;
  
  free(c_prior);
  c_prior = NULL;
  
  free(inv_width);
  inv_width = NULL;
}
